/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/engd_parser/include/ruleInversionMaskTableEntry.H $ */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2015                                                         */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
#ifndef ruleInversionMaskTableEntry_H
#define ruleInversionMaskTableEntry_H

/**
 * Each scanring has an inversionmask which needs to be applied for
 * every access.  This table contains all the inversion masks needed
 * by the particular chip indicated by the header and/or filename.  There
 * are no real table entries per se; masks are accessed simply by using
 * an offset into the file and a length value.
 *
 * There will be one inversionmask table for every chipid/ec.
 *
 * If the size of this table becomes an issue, we have the option to
 * store it in a compressed form. In this case we would store only the
 * places where a 1 should be set.
 */

//----------------------------------------------------------------------
// Includes
//----------------------------------------------------------------------

#include <ruleTableEntry.H>
#include <ecmdDataBuffer.H>
#define HOM_INVALID_UNIT 0x00000000
#define HOM_INVERSIONMASK_TABLE 0x00F0000F
//----------------------------------------------------------------------
// Class Definitions
//----------------------------------------------------------------------

class ruleInversionMaskTableEntry : public ruleTableEntry
{

    public:
        /**
         * Default constructor.
         */
        ruleInversionMaskTableEntry();

        /**
         * Copy Constructor.
         */
        ruleInversionMaskTableEntry(const ruleInversionMaskTableEntry&);

        /**
         * Assignment operator.
         */
        void operator = (const ruleInversionMaskTableEntry&);

        /**
         * Destructor.
         */
        virtual ~ruleInversionMaskTableEntry();

        /**
         * Opens the specified file and sets the file pointer for this
         * class.  Returns a status code.
         *
         * This is basically identical to the version implemented in ruleTableEntry,
         * except this version does not do the HOM_LAST_UNITID check that the other
         * one does, nor does it count rows since there aren't really any.
         *
         * @param string i_filename  Name of the table file to open
         * @param bool i_useRegistry Indicates if the registry path should be used
         *
         * @return int Error handle
         */
        virtual int openFile(std::string i_filename);

        /**
         * This function is NOT IMPLEMENTED.  There is no nead to copy table entry
         * data for inversion masks.
         *
         * @param uint32_t i_filePos Position in the file to load from
         *
         * @return int Error handle
         */
        virtual int copyData(uint32_t i_filePos);

        /**
         * This is a virtual function.  This function is NOT IMPLEMENTED here.  There
         * is no nead to advance table entries for inversion masks.
         *
         * @param uint32_t i_currPos Seek position of the current entry
         *
         * @return uint32_t Seek position of the next entry
         */
        virtual uint32_t advanceToNextEntry(uint32_t i_currPos);

        /**
         * Returns the inversion mask in a bit buffer.
         *
         * @param uint32_t i_offset       Offset to the inv mask being requested
         * @param uint23_t i_length       Length of the provided buffer
         * @param uint23_t i_optimized    optimized scanring storage
         * @param ecmdDataBuffer* o_buffer Buffer to write the inversion mask to
         *
         * @return int Error handle
         */
        int getInvMask(uint32_t i_offset, uint32_t i_length, uint8_t i_optimized,
                       ecmdDataBuffer* o_buffer);

        int copyVersionData(ruleFile* i_fileHandle, uint32_t& o_length);
};

#endif
