# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/engd_parser/engd_parser.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2016
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG
MODULE = engd_parser
lib$(MODULE)_LDFLAGS   += -ldl
lib$(MODULE)_LIBPATH=$(LIBPATH)/ifCompiler
lib$(MODULE)_DEPLIBS   += fapi2_ifcompiler
lib$(MODULE)_EXTRALIBS += $(ECMD_REQUIRED_LIBS)

SRC_FILES +=  $(notdir $(wildcard $(ROOTPATH)/tools/ifCompiler/engd_parser/src/*.C))

OBJS += $(patsubst %.C,%.o,$(SRC_FILES))

$(call ADD_MODULE_INCDIR,$(MODULE),$(ECMD_PLAT_INCLUDE))
$(call ADD_MODULE_INCDIR,$(MODULE),$(ROOTPATH)/tools/ifCompiler/engd_parser/include/)
$(call ADD_MODULE_INCDIR,$(MODULE),$(ROOTPATH)/hwpf/fapi2/include/)
$(call ADD_MODULE_INCDIR,$(MODULE),$(ROOTPATH)/tools/ifCompiler/plat/)
$(call ADD_MODULE_INCDIR,$(MODULE),$(GENPATH)/fapi2_ifCompiler)
$(call ADD_MODULE_SRCDIR,$(MODULE),$(ROOTPATH)/tools/ifCompiler/engd_parser/src/)
$(call BUILD_MODULE)
