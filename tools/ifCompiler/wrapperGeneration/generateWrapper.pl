#!/usr/bin/perl
# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/wrapperGeneration/generateWrapper.pl $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2019
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

use strict;
use File::Basename qw(fileparse);
use File::Path qw(make_path);
use Getopt::Long;
use Data::Dumper qw(Dumper);

use lib "$ENV{'PERLMODULES'}";
use TargetString;

my $chip                 = undef;
my $chipId               = undef;
my $ec                   = undef;
my $outputDir            = undef;
my $mode                 = undef;
my $debug                = undef;
my %scan_inputs          = ();
my %ana_bndy_scan_inputs = ();
my $rootpath             = "../..";

my %RING_ID_FILE = (
    "centaur" => "cen_ringId.H",
    "p9"      => "p9_ringId.H",
);

my %CHIP_TYPE = (
    "cen" => "fapi2::ENUM_ATTR_NAME_CENTAUR",
    "p9n" => "fapi2::ENUM_ATTR_NAME_NIMBUS",
    "p9c" => "fapi2::ENUM_ATTR_NAME_CUMULUS",
    "p9a" => "fapi2::ENUM_ATTR_NAME_AXONE",
);

GetOptions(
    "chip=s"       => \$chip,
    "chipId=s"     => \$chipId,
    "ec=s"         => \$ec,
    "mode=s"       => \$mode,
    "debug=s"      => \$debug,
    "output-dir=s" => \$outputDir
);

my $numArgs = $#ARGV + 1;

#print out all the input arguments for debug purposes

if ($debug)
{
    print "chip         = $chip\n";
    print "chipId       = $chipId\n";
    print "ec           = $ec\n";
    print "mode         = $mode\n";
    print "debug        = $debug\n";
    print "output-dir   = $outputDir\n";

    foreach my $argNum ( 0 .. $#ARGV )
    {
        my $temp = $ARGV[$argNum];
        $temp =~ s/\@/ /g;
        print "$temp\n";
    }
}

#Error checking on command-line argument
if (   ( !defined $chip )
    || ( !defined $chipId )
    || ( !defined $ec )
    || ( !defined $mode )
    || ( !defined $outputDir )
    || ( $numArgs < 1 ) )
{
    print "Usage: generateWrapper.pl --chip=<chipid> --ec=<chipec>
        --output-dr=<output dir> <SCAN_INPUT lines from scan_procedures.mk>\n";
    exit(1);
}

#globals
my $attrPath = $rootpath . "/chips/$chip/initfiles/attribute_ovd/";

#call the main function
main();

###############################################################################
# Subroutines Begin
###############################################################################
sub main
{

    #open the *wrapper.C file for writing
    my $outFile = $chipId . "_" . $ec . "_" . $mode . "_wrapper";
    open( OUTFILE, ">", $outputDir . "/" . $outFile . ".C" );
    parseInput();
    displayWrapperBeginning();
    processAllTags( $chipId, $ec );
    print OUTFILE "}\n";
    close(OUTFILE);

    #open *wrapper.mk for writing
    open( OUT_MK_FILE, ">", $outputDir . "/" . $outFile . ".mk" );
    writeMkFile($outFile);

}

#sort a name by numbers
sub sort_by_number
{
    $a =~ /(\d+)/;
    my $numa = $1;
    $b =~ /(\d+)/;
    my $numb = $1;

    return $numa <=> $numb;
}

sub parseInput
{
    foreach my $argNum ( 0 .. $#ARGV )
    {
        my $argStr = $ARGV[$argNum];

        #Use "@" delimiter to split the string
        my @splitStr = split /\@/, $argStr;

        #parse the c_clk initfiles into a separate hash, they require special processing
        if ( $splitStr[1] =~ m/c_clk/ )
        {
            $ana_bndy_scan_inputs{ $splitStr[0] }{ $splitStr[4] }{ $splitStr[1] }{target} = $splitStr[2];
            if ( exists $splitStr[3] )
            {
                $ana_bndy_scan_inputs{ $splitStr[0] }{ $splitStr[4] }{ $splitStr[1] }{attr} = $attrPath . $splitStr[3];
            }
        }
        else
        {
            $scan_inputs{ $splitStr[0] }{ $splitStr[4] }{ $splitStr[1] }{target} = $splitStr[2];
            if ( exists $splitStr[3] )
            {
                $scan_inputs{ $splitStr[0] }{ $splitStr[4] }{ $splitStr[1] }{attr} = $attrPath . $splitStr[3];
            }
        }
    }
    print Dumper ( \%scan_inputs )          if ($debug);
    print Dumper ( \%ana_bndy_scan_inputs ) if ($debug);
}

sub addIncludeFiles
{
    my %procedures = ();

    my $inputs   = shift;
    my $includes = shift;

    my %scan_inputs = %{$inputs};

    #create a list of procedures and create the #include statements for each
    foreach my $dir ( keys %scan_inputs )
    {
        foreach my $tag ( keys %{ $scan_inputs{$dir} } )
        {
            foreach my $initfile ( keys %{ $scan_inputs{$dir}{$tag} } )
            {
                # If initfile starts with chipId or chip + '.'
                #   chipId : p9n. p9c.
                #   chip   : p9.  centaur.
                if (   ( index( $initfile, ( $chipId . '.' ) ) == 0 )
                    || ( index( $initfile, ( $chip . '.' ) ) == 0 ) )
                {
                    my $procedureName = $initfile;
                    $procedureName =~ s/.initfile//;
                    $procedureName =~ s/\./_/g;

                    #weed out the duplicate .H file names
                    if ( !exists( $procedures{$procedureName} ) )
                    {
                        $$includes .= "\#include <$procedureName.H>\n";

                        # add this one to the hash so we will skip it next
                        # time through
                        $procedures{$procedureName} = 1;
                    }
                }
            }
        }
    }
}

sub displayWrapperBeginning
{

    my $includes = "\#include <iostream>\n";
    $includes .= "\#include <parseAttrOvd.H>\n";
    $includes .= "\#include <SpyInterface.H>\n";
    $includes .= "\#include <fapi2.H>\n";

    # include the correct ring id file base on chip name
    $includes .= "\#include <$RING_ID_FILE{$chip}>\n";
    if ( $chip eq "p9" )
    {
        $includes .= "\#include <p9_frequency_buckets.H>\n";
    }

    addIncludeFiles( \%scan_inputs,          \$includes );
    addIncludeFiles( \%ana_bndy_scan_inputs, \$includes );

    print OUTFILE $includes;
    print OUTFILE "\n";
    displyDumpRings();
    print OUTFILE "namespace fapi2 {\n    ReturnCode current_err\;\n}\n";
    print OUTFILE "\n\nint main (void)\n";
    print OUTFILE "{\n\n";
    print OUTFILE "    fapi2::ReturnCode l_rc = fapi2::FAPI2_RC_SUCCESS\;\n";
    print OUTFILE "    fapi2::TgtAttrMap_t l_attrData\;\n";
    print OUTFILE TargetString::parse( "k0:n0:s0:", "_system" );

    if ( ( $mode eq "hw" ) || ( $mode eq "HW" ) )
    {
        displayFapiAttrSet( "ATTR_IS_SIMULATION", "l_tgt_system", "0" );
    }
    elsif ( ( $mode eq "sim" ) || ( $mode eq "SIM" ) )
    {
        displayFapiAttrSet( "ATTR_IS_SIMULATION", "l_tgt_system", "1" );
    }
    else
    {
        print "ERROR: Unsupported mode $mode\n";
        die;
    }

    if ( $chip eq "centaur" )
    {
        print OUTFILE TargetString::parse( "k0:n0:s0:centaur:pall", "_membuf" );
        displayFapiAttrSet( "ATTR_NAME", "l_tgt_membuf", $CHIP_TYPE{$chipId} );
        displayFapiAttrSet( "ATTR_EC",   "l_tgt_membuf", "0x$ec" );
    }
    elsif ( $chip eq "p9" )
    {
        print OUTFILE TargetString::parse( "k0:n0:s0:p9n:pall", "_proc" );
        displayFapiAttrSet( "ATTR_NAME", "l_tgt_proc", $CHIP_TYPE{$chipId} );
        displayFapiAttrSet( "ATTR_EC",   "l_tgt_proc", "0x$ec" );
    }

    print OUTFILE "\nENGD::ChipEngData& l_engData = ENGD::ChipEngData::getInstance( $CHIP_TYPE{$chipId}, 0x$ec );\n";
    print OUTFILE "\nl_engData.init();\n";

}

sub processAllTags
{
    my ( $chipId, $ec ) = @_;
    foreach my $dir ( keys %scan_inputs )
    {
        foreach my $tag ( keys %{ $scan_inputs{$dir} } )
        {
            print OUTFILE "    SPY::RingFactory::instance().clear()\;\n";

            processAllInitfiles( \%{ $scan_inputs{$dir}{$tag} } );

            my $outputPath = $rootpath . "/output/gen/rings/$mode/$chipId/$ec/$dir/";
            my ( $file, $filePath ) = fileparse $outputPath;
            if ( !-d $filePath )
            {
                make_path $filePath or die "Failed to create path: $dir";
            }
            print OUTFILE "    dumpRings(std::string(\"$filePath\"),std::string(\"$tag\"),\n";
            print OUTFILE "              l_ATTR_NAME, l_ATTR_EC);\n\n";
        }
    }

    foreach my $dir ( keys %ana_bndy_scan_inputs )
    {

        #for each phase (ipl_base,runtime_base etc)  I need a flushed set of rings
        print OUTFILE "    SPY::RingFactory::instance().clear()\;\n";

        #sort the tags (bucket_x) by bucket number so they will be run sequentially
        my @sorted_tags = sort sort_by_number keys %{ $ana_bndy_scan_inputs{$dir} };

        foreach my $tag (@sorted_tags)
        {

            # for each bucket I need to keep the ring but flush the bits modified buffer
            print OUTFILE "    SPY::RingFactory::instance().resetBitsModified()\;\n";

            processEqAnaBndyInitfiles( \%{ $ana_bndy_scan_inputs{$dir}{$tag} } );

            my $outputPath = $rootpath . "/output/gen/rings/$mode/$chipId/$ec/$dir/";
            my ( $file, $filePath ) = fileparse $outputPath;
            if ( !-d $filePath )
            {
                make_path $filePath or die "Failed to create path: $dir";
            }

            #dump rings after each initfile
            print OUTFILE "    dumpRings(std::string(\"$filePath\"),std::string(\"$tag\"),\n";
            print OUTFILE "              l_ATTR_NAME, l_ATTR_EC);\n\n";
        }
    }

}

sub processEqAnaBndyInitfiles
{
    my ($i_input) = @_;

    foreach my $initfile ( keys %{$i_input} )
    {

        #print "initfile $initfile
        #target: $$i_input{$initfile}{target}
        #attr: $$i_input{$initfile}{attr}\n";

        my $procedureName = $initfile;
        $procedureName =~ s/.initfile//;
        $procedureName =~ s/\./_/g;

        #begin execution of this initfile
        print OUTFILE "    {\n";

        #display Targets
        my @targetList = split /,/, $$i_input{$initfile}{target};
        foreach my $tgtNum ( 0 .. $#targetList )
        {
            print OUTFILE "        ";
            print OUTFILE TargetString::parse( $targetList[$tgtNum], $tgtNum );
            print OUTFILE "\n";
        }

        #parseAttrOvd
        if ( defined $$i_input{$initfile}{attr} )
        {
            my ( $file, $filePath ) = fileparse $$i_input{$initfile}{attr};
            if ( -e $$i_input{$initfile}{attr} )
            {
                displayParseAttrOvd( $$i_input{$initfile}{attr} );
            }
            else
            {
                die "$$i_input{$initfile}{attr} does not exist";
            }
        }

        #call FAPI HWP
        my $fapiExecHWP .= "        FAPI_EXEC_HWP(l_rc," . $procedureName;
        foreach my $tgtNum ( 0 .. $#targetList )
        {
            $fapiExecHWP .= ",l_tgt" . $tgtNum;
        }
        $fapiExecHWP .= ");\n";

        $fapiExecHWP .= "        if (l_rc)\n";
        $fapiExecHWP .= "        {\n";
        $fapiExecHWP .= "            FAPI_ERR(\"Error executing $procedureName\")\;\n";
        $fapiExecHWP .= "            exit(l_rc)\;\n";
        $fapiExecHWP .= "        }\n";
        print OUTFILE $fapiExecHWP;
        print OUTFILE "    }\n";

    }
}

sub processAllInitfiles
{
    my ($i_input) = @_;

    foreach my $initfile ( keys %{$i_input} )
    {
        # Skip initfiles that don't belong to this chip
        if (   ( index( $initfile, ( $chipId . '.' ) ) != 0 )
            && ( index( $initfile, ( $chip . '.' ) ) != 0 ) )
        {
            next;
        }

        #print "initfile $initfile
        #target: $$i_input{$initfile}{target}
        #attr: $$i_input{$initfile}{attr}\n";

        my $procedureName = $initfile;
        $procedureName =~ s/.initfile//;
        $procedureName =~ s/\./_/g;

        #begin execution of this initfile
        print OUTFILE "    {\n";

        #display Targets
        my @targetList = split /,/, $$i_input{$initfile}{target};
        foreach my $tgtNum ( 0 .. $#targetList )
        {
            print OUTFILE "        ";
            print OUTFILE TargetString::parse( $targetList[$tgtNum], $tgtNum );
            print OUTFILE "\n";
        }

        #parseAttrOvd
        if ( defined $$i_input{$initfile}{attr} )
        {
            my ( $file, $filePath ) = fileparse $$i_input{$initfile}{attr};
            if ( -e $$i_input{$initfile}{attr} )
            {
                displayParseAttrOvd( $$i_input{$initfile}{attr} );
            }
            elsif ( $file =~ m/nest_bucket(\w).txt/ )
            {

                #$1 is the index into the array
                my $array_idx = $1 - 1;
                print OUTFILE "        ";
                print OUTFILE TargetString::parse( "k0:n0:s0", "Nest$1" );
                print OUTFILE "\n";
                print OUTFILE "fapi2::ATTR_FREQ_PB_MHZ_Type l_nest_freq = NEST_PLL_FREQ_LIST[$array_idx];";
                print OUTFILE "\n";
                print OUTFILE "FAPI_ATTR_SET";
                print OUTFILE "(fapi2::ATTR_FREQ_PB_MHZ,l_tgtNest$1,l_nest_freq);\n";

            }
            elsif ( $file =~ m/mem_bucket(\w).txt/ )
            {
                my $array_idx = $1 - 1;
                print OUTFILE "        ";
                print OUTFILE TargetString::parse( "k0:n0:s0:p9n.mcbist", "Mem$1" );
                print OUTFILE "\n";
                print OUTFILE "fapi2::ATTR_MSS_FREQ_Type l_mem_freq = MEM_PLL_FREQ_LIST[$array_idx];";
                print OUTFILE "\n";
                print OUTFILE "FAPI_ATTR_SET";
                print OUTFILE "(fapi2::ATTR_MSS_FREQ,l_tgtMem$1,l_mem_freq);\n";

            }
            elsif ( $file =~ m/omi_bucket(\w).txt/ )
            {
                my $array_idx = $1 - 1;
                print OUTFILE "        ";
                print OUTFILE TargetString::parse( "k0:n0:s0:p9a", "Omi$1" );
                print OUTFILE "\n";
                print OUTFILE "fapi2::ATTR_FREQ_OMI_MHZ_Type l_omi_freq = OMI_PLL_FREQ_LIST[$array_idx].omifreq;";
                print OUTFILE "\n";
                print OUTFILE "fapi2::ATTR_OMI_PLL_VCO_Type l_omi_vco = OMI_PLL_FREQ_LIST[$array_idx].vco;";
                print OUTFILE "\n";
                print OUTFILE "FAPI_ATTR_SET";
                print OUTFILE "(fapi2::ATTR_FREQ_OMI_MHZ,l_tgtOmi$1,l_omi_freq);\n";
                print OUTFILE "FAPI_ATTR_SET";
                print OUTFILE "(fapi2::ATTR_OMI_PLL_VCO,l_tgtOmi$1,l_omi_vco);\n";

            }
            elsif ( $file =~ m/obus_bucket(\w).txt/ )
            {
                my $array_idx = $1 - 1;
                my $chipId_uc = uc($chipId);
                print OUTFILE "        ";
                print OUTFILE TargetString::parse( "k0:n0:s0", "Obus$1" );
                print OUTFILE "\n";
                print OUTFILE
                    "fapi2::ATTR_FREQ_A_MHZ_Type l_obus_freq = OBUS_PLL_FREQ_LIST_${chipId_uc}_${ec}[$array_idx];";
                print OUTFILE "\n";
                print OUTFILE "FAPI_ATTR_SET";
                print OUTFILE "(fapi2::ATTR_FREQ_A_MHZ,l_tgtObus$1,l_obus_freq);\n";

            }
            else
            {
                die "$$i_input{$initfile}{attr} does not exist";
            }
        }

        #call FAPI HWP
        my $fapiExecHWP .= "        FAPI_EXEC_HWP(l_rc," . $procedureName;
        foreach my $tgtNum ( 0 .. $#targetList )
        {
            $fapiExecHWP .= ",l_tgt" . $tgtNum;
        }
        $fapiExecHWP .= ");\n";

        $fapiExecHWP .= "        if (l_rc)\n";
        $fapiExecHWP .= "        {\n";
        $fapiExecHWP .= "            FAPI_ERR(\"Error executing $procedureName\")\;\n";
        $fapiExecHWP .= "            exit(l_rc)\;\n";
        $fapiExecHWP .= "        }\n";
        print OUTFILE $fapiExecHWP;
        print OUTFILE "    }\n";

    }
}

sub displyDumpRings
{

    my $dumpRing = "void dumpRings (std::string i_path, std::string i_tag,\n";
    $dumpRing .= "                uint8_t i_chipId, uint32_t i_chipEc)\n";
    $dumpRing .= "{\n";
    $dumpRing .= "    for(int i = 0; i < RingID::NUM_RING_IDS; i++)\n";
    $dumpRing .= "    {\n";
    $dumpRing .= "        std::string l_ringName = std::string(RING_PROPERTIES[i].iv_name)\;\n";
    $dumpRing .= "        if (!l_ringName.empty())\n";
    $dumpRing .= "        {\n";
    $dumpRing .= "            SPY::RingFactory::instance().dump(l_ringName,\n";
    $dumpRing .= "            i_path, i_tag, i_chipId, i_chipEc, SPY::STANDARD_RINGS)\;\n";
    $dumpRing .= "        }\n";
    $dumpRing .= "    }\n";
    $dumpRing .= "}\n";
    print OUTFILE $dumpRing;
}

sub displayParseAttrOvd
{
    my ($i_attrFile) = @_;

    my $attrOvd = "        bool l_success = ";
    $attrOvd .= "parseAttrOverride::attrTextToData(";
    $attrOvd .= "\"$i_attrFile\", l_attrData)\;\n";
    $attrOvd .= "        if (!l_success)\n";
    $attrOvd .= "        {\n";
    $attrOvd .= "            std::cerr << \"initProcedure_main> ERROR ";
    $attrOvd .= "parsing attribute override file: \" << ";
    $attrOvd .= "\"$i_attrFile\" << std::endl\;\n";
    $attrOvd .= "            exit(-1)\;\n";
    $attrOvd .= "        }\n";

    if ($debug)
    {
        $attrOvd .= "   std::cout << \"Calling plat_init for $mode:";
        $attrOvd .= " $i_attrFile\" << std::endl;\n";
    }

    $attrOvd .= "        fapi2::plat_init(l_attrData)\;\n";
    print OUTFILE $attrOvd;
}

sub displayFapiAttrSet
{
    my ( $attribute, $target, $value ) = @_;
    my $attr_type = "fapi2::" . $attribute . "_Type";
    print OUTFILE "$attr_type l_$attribute = $value;\n";
    print OUTFILE "l_rc = ";
    print OUTFILE "fapi2::setAttrOvdValue(fapi2::$attribute, $target,";
    print OUTFILE "&l_$attribute, sizeof($attr_type));\n";
    print OUTFILE "if (l_rc)\n";
    print OUTFILE "{\n";
    print OUTFILE "    std::cerr << \"ERROR setting $attribute to $value\"";
    print OUTFILE " << std::endl;\n";
    print OUTFILE "    exit(-1);\n";
    print OUTFILE "}\n\n";
}

sub addProcedureDeps
{
    my $inputs     = shift;
    my %procedures = ();

    my %scan_inputs = %{$inputs};

    foreach my $dir ( keys %scan_inputs )
    {
        foreach my $tag ( keys %{ $scan_inputs{$dir} } )
        {
            foreach my $initfile ( keys %{ $scan_inputs{$dir}{$tag} } )
            {
                # If initfile starts with chipId or chip + '.'
                #   chipId : p9n. p9c.
                #   chip   : p9.  centaur.
                if (   ( index( $initfile, ( $chipId . '.' ) ) == 0 )
                    || ( index( $initfile, ( $chip . '.' ) ) == 0 ) )
                {
                    my $procedureName = $initfile;
                    $procedureName =~ s/.initfile//;
                    $procedureName =~ s/\./_/g;

                    #weed out the duplicate .H file names
                    if ( !exists( $procedures{$procedureName} ) )
                    {
                        print OUT_MK_FILE "\$(EXE)_DEPLIBS+=$procedureName" . "_ifCompiler\n";

                    #add the proceudre dependancies for the chipId specific wrapper.. no need to compile all the modules
                        print OUT_MK_FILE $chipId . "."
                            . $mode
                            . "_image_MODULE_TARGETS+=\$(LIBPATH)/ifCompiler/lib"
                            . $procedureName
                            . "_ifCompiler.a\n";

                        # add this one to the hash so we will skip it next
                        # time through
                        $procedures{$procedureName} = 1;
                    }

                }
            }
        }
    }
}

sub writeMkFile
{
    my ($outFile) = @_;
    print OUT_MK_FILE "EXE=$outFile\n";
    print OUT_MK_FILE "\$(EXE)_TARGET=HOST\n";
    print OUT_MK_FILE "OBJS+=$outFile" . ".o\n";
    print OUT_MK_FILE "\$(EXE)_COMMONFLAGS+=-DFAPI_SUPPORT_SPY_AS_STRING\n";
    print OUT_MK_FILE "\$(EXE)_COMMONFLAGS+=-DPLAT_NO_THREAD_LOCAL_STORAGE\n";
    print OUT_MK_FILE "\$(EXE)_LIBPATH+=\$(LIBPATH)/ifCompiler\n";
    print OUT_MK_FILE "\$(EXE)_EXTRALIBS+=\$(ECMD_REQUIRED_LIBS)\n";
    print OUT_MK_FILE "\$(EXE)_DEPLIBS+=engd_parser parseAttrOvd fapi2_ifcompiler\n";

    #add a DEPLIBS stanza for each procedure pass in
    addProcedureDeps( \%scan_inputs );
    addProcedureDeps( \%ana_bndy_scan_inputs );

    print OUT_MK_FILE "\$(call ADD_EXE_SRCDIR,\$(EXE),\$(ROOTPATH)/tools/ifCompiler)\n";
    print OUT_MK_FILE "\$(call ADD_EXE_SRCDIR,\$(EXE),\$(GENPATH)/fapi2_ifCompiler)\n";
    print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(GENPATH)/initfiles)\n";
    print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/tools/ifCompiler/plat)\n";
    if ( $chip eq "centaur" )
    {
        print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/chips/centaur/utils/imageProcs)\n";
        print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/chips/p9/utils/imageProcs)\n";
    }
    else
    {
        print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/chips/p9/utils/imageProcs)\n";
    }

    # Note: This ffdc is common for FAPI2, even though it's placed under chips/p9/... directory.
    # Leave it outside of the above block
    print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/chips/p9/procedures/hwp/ffdc)\n";

    print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/tools/ifCompiler/engd_parser/include)\n";
    print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/hwpf/fapi2/include)\n";
    print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ECMD_PLAT_INCLUDE))\n";
    print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/chips/p9/common/include)\n";
    print OUT_MK_FILE "\$(call ADD_EXE_INCDIR,\$(EXE),\$(ROOTPATH)/chips/common/utils/imageProcs/)\n";
    print OUT_MK_FILE "\$(call BUILD_EXE)\n";
    close(OUT_MK_FILE);
}
