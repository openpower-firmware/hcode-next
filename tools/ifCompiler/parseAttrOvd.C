/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/parseAttrOvd.C $                             */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2015,2018                                                    */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
/**
 * @Description - This file performs the operation of converting a text file
 *                containing various attributes into their binary
 *                representations and outputing that data into a .bin file. The
 *                program uses a lot of the functionality/logic present in
 *                /hostboot/src/usr/hwpf/plat/fapiPlatAttrOverrideSync in order
 *                to parse the input attribute text file into corresponding
 *                data. This tool takes in an attribute override text file
 *                as input and outputs an ECC protected binary blob containing
 *                the data for the given input attribute overrides.
 *
 *                output file name: attrOverride.bin
 */
//******************************************************************************
// Includes
//******************************************************************************

#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <stdint.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <endian.h>
#include <unistd.h>
#include <target_types.H>
#include <iostream>

#include "parseAttrOvd.H"

namespace AttrOverrideSyncConstants
{
// Constants used for processing all attribute related text files
const size_t MIN_ATTRIBUTE_SIZE = 32; //Every attribute has AT LEAST 32 bytes
const size_t ATTRIBUTE_HEADER_SIZE = 16;
//******************************************************************************
// Constants used for processing FAPI Attribute Text files
// See the header file for the format
//******************************************************************************
const char* const ATTR_FILE_ATTR_START_STR = "ATTR_";
const char* const ATTR_FILE_TARGET_HEADER_STR = "target";
const char* const ATTR_FILE_TARGET_EXT_HEADER_STR = "target = k0";
const char* const ATTR_FILE_TARGET_EXT_FOOTER_STR = ":s0:";
const char* const ATTR_CONST = "CONST";
const char* const TARGET_NODE_HEADER_STR = ":n";
const char* const TARGET_POS_HEADER_STR = ":p";
const char* const TARGET_UNIT_POS_HEADER_STR = ":c";
const char* const TARGET_NODE_ALL_STR = "all";
const char* const TARGET_POS_ALL_STR = "all";


// Used to translate target strings in FAPI Attribute Info files to the value
// in a FAPI or TARG Layer AttributeTanks
struct TargStrToType
{
    const char* iv_pString;
    const uint32_t iv_fapiType;
};


TargStrToType CHIP_TYPE_TARG_STR_TO_TYPE [] =
{
    {"p9n",              fapi2::TARGET_TYPE_PROC_CHIP},
    {"p9c",              fapi2::TARGET_TYPE_PROC_CHIP},
    {"p9a",              fapi2::TARGET_TYPE_PROC_CHIP},
    {"pu",               fapi2::TARGET_TYPE_PROC_CHIP},
    {"centaur",          fapi2::TARGET_TYPE_MEMBUF_CHIP}
};

TargStrToType CHIP_UNIT_TYPE_TARG_STR_TO_TYPE [] =
{
    {"c"       , fapi2::TARGET_TYPE_CORE},
    {"ex"      , fapi2::TARGET_TYPE_EX},
    {"eq"      , fapi2::TARGET_TYPE_EQ},
    {"mcs"     , fapi2::TARGET_TYPE_MCS},
    {"mca"     , fapi2::TARGET_TYPE_MCA},
    {"mcbist"  , fapi2::TARGET_TYPE_MCBIST},
    {"xbus"    , fapi2::TARGET_TYPE_XBUS},
    {"obus"    , fapi2::TARGET_TYPE_OBUS},
    {"obrick"  , fapi2::TARGET_TYPE_OBUS_BRICK},
    {"sbe"     , fapi2::TARGET_TYPE_SBE},
    {"ppe"     , fapi2::TARGET_TYPE_PPE},
    {"perv"    , fapi2::TARGET_TYPE_PERV},
    {"pec"     , fapi2::TARGET_TYPE_PEC},
    {"phb"     , fapi2::TARGET_TYPE_PHB},
    {"capp"    , fapi2::TARGET_TYPE_CAPP},
    {"mba"     , fapi2::TARGET_TYPE_MBA},
    {"dimm"    , fapi2::TARGET_TYPE_DIMM},
    {"dmi"     , fapi2::TARGET_TYPE_DMI},
    {"mi"      , fapi2::TARGET_TYPE_MI},
    {"mc"      , fapi2::TARGET_TYPE_MC},
};

bool operator==(const TargStrToType& i, const std::string& v)
{
    return 0 == strcmp(v.c_str(), i.iv_pString);
}

const char* const ATTR_INFO_FILE_UINT8_STR = "u8";
const char* const ATTR_INFO_FILE_UINT16_STR = "u16";
const char* const ATTR_INFO_FILE_UINT32_STR = "u32";
const char* const ATTR_INFO_FILE_UINT64_STR = "u64";

// Constants for various fields in the target declaration
static const uint16_t ATTR_POS_NA = 0xffff;      // iv_pos N/A
static const uint8_t  ATTR_UNIT_POS_NA = 0xff;   // iv_unitPos N/A
static const uint8_t  ATTR_NODE_NA = 0xf;        // iv_node N/A

}

template <typename T>
int compareAttribute(const T& l, const T& r)
{
    return strcmp(l.iv_name, r.iv_name);
}

template <typename T> bool operator<(const T&, const T&);
template <>
bool operator< <AttributeData>(const AttributeData& l, const AttributeData& r)
{
    return compareAttribute(l, r) < 0;
}

template <>
bool operator< <AttributeEnum>(const AttributeEnum& l, const AttributeEnum& r)
{
    return compareAttribute(l, r) < 0;
}
template <typename T>
const T* findAttribute(const T* array,
                       size_t arraySize,
                       const char* attrName)
{
    T constant;
    constant.iv_name = attrName;

    const T* element =
        std::lower_bound(&array[0], &array[arraySize], constant);

    if ((&array[arraySize] == element) ||
        (0 != compareAttribute(*element, constant)))
    {
        return NULL;
    }

    return element;
}

using namespace AttrOverrideSyncConstants;

namespace parseAttrOverride
{
//******************************************************************************
bool attrFileIsAttrLine(
    const std::string& i_line,
    std::string& o_attrString)
{
    /*
     * e.g. "target = k0:n0:s0:centaur.mba:pall:call" - false
     *      "ATTR_MSS_DIMM_MFG_ID_CODE[0][0] u32[2][2] 0x12345678" - true
     */
    bool l_isAttrLine = false;

    if (0 == i_line.find(ATTR_FILE_ATTR_START_STR))
    {
        // The attribute ID string terminates with either '[' or ' '
        size_t l_pos = i_line.find_first_of("[ ");

        if (l_pos != std::string::npos)
        {
            o_attrString = i_line.substr(0, l_pos);
            l_isAttrLine = true;
        }
    }

    return l_isAttrLine;
}



//******************************************************************************
bool attrFileIsTargLine(
    const std::string& i_line)
{
    /*
     * e.g. "target = k0:n0:s0:centaur.mba:pall:call" - true
     *      "ATTR_MSS_DIMM_MFG_ID_CODE[0][0] u32[2][2] 0x12345678" - false
     */
    return 0 == i_line.find(ATTR_FILE_TARGET_HEADER_STR);
}


//******************************************************************************
bool attrFileAttrLineToFields(
    const std::string& i_line,
    std::string& o_attrString,
    size_t (& o_dims)[ATTR_MAX_DIMS],
    std::string& o_valStr,
    bool& o_const)
{
    /*
     * e.g. "ATTR_MSS_DIMM_MFG_ID_CODE[0][1] u32[2][2] 0x12345678"
     * - o_attrString = "ATTR_MSS_DIMM_MFG_ID_CODE"
     * - o_dims = {0, 1, 0, 0}
     * - o_valStr = "0x12345678"
     * - o_const = false
     */
    bool l_success = false;
    bool l_break = false;
    o_const = false;
    size_t l_pos1 = 0;
    size_t l_pos2 = 0;

    for (size_t i = 0; i < ATTR_MAX_DIMS; i++)
    {
        o_dims[i] = 0;
    }

    // Copy input string into a local string and strip of any newline
    std::string l_line(i_line);


    if (l_line[l_line.size() - 1] == '\n')
    {
        l_line = l_line.substr(0, l_line.size() - 1);
    }

    do
    {
        // Find the first field: attribute string
        l_pos2 = l_line.find_first_of(" \t");

        if (l_pos2 == std::string::npos)
        {
            printf(
                "attrFileAttrLineToFields:"
                " Could not find end of attr str in '%s'\n",
                l_line.c_str());
            break;
        }

        // Found the attribute-string
        std::string l_attrString = l_line.substr(0, l_pos2);


        // Find if the attribute string contains array dimensions
        size_t l_pos = l_attrString.find('[');
        o_attrString = l_attrString.substr(0, l_pos);

        size_t l_dim = 0;

        while ((l_pos != std::string::npos) && (l_dim < ATTR_MAX_DIMS))
        {
            l_attrString = l_attrString.substr(l_pos + 1);
            o_dims[l_dim++] = strtoul(l_attrString.c_str(), NULL, 0);

            if(l_dim > ATTR_MAX_DIMS)
            {
                printf("MAX_DIMS exceeded! Exiting... "
                       "attrTextToBinaryBlob::attrFileAttrLineToFields");
                l_break = true;
                break;
            }

            l_pos = l_attrString.find('[');
        }

        if( l_break )
        {
            break;
        }

        // Find the second field: type (optional) or value
        l_pos1 = l_line.find_first_not_of(" \t", l_pos2);

        if (l_pos1 == std::string::npos)
        {
            printf(
                "attrFileAttrLineToFields:"
                " Could not find start of second field in '%s'\n",
                l_line.c_str());
            break;
        }

        l_pos2 = l_line.find_first_of(" \t", l_pos1);

        if (l_pos2 == std::string::npos)
        {
            // The second and last string must be the value string
            o_valStr = l_line.substr(l_pos1);
            l_success = true;
            break;
        }

        // Found the second field
        o_valStr = l_line.substr(l_pos1, l_pos2 - l_pos1);

        // If the second field is the optional and unused type field then
        // the next field is the val string
        if ( (o_valStr.find(ATTR_INFO_FILE_UINT8_STR) != std::string::npos) ||
             (o_valStr.find(ATTR_INFO_FILE_UINT16_STR) != std::string::npos) ||
             (o_valStr.find(ATTR_INFO_FILE_UINT32_STR) != std::string::npos) ||
             (o_valStr.find(ATTR_INFO_FILE_UINT64_STR) != std::string::npos) )
        {
            l_pos1 = l_line.find_first_not_of(" \t", l_pos2);

            if (l_pos1 == std::string::npos)
            {
                printf(
                    "attrFileAttrLineToFields:"
                    " Could not find start of val field in '%s'\n",
                    l_line.c_str());
                break;
            }

            l_pos2 = l_line.find_first_of(" \t", l_pos1);

            if (l_pos2 == std::string::npos)
            {
                // The third and last string must be the value string
                o_valStr = l_line.substr(l_pos1);
                l_success = true;
            }
            else
            {
                o_valStr = l_line.substr(l_pos1, l_pos2 - l_pos1);
                l_success = true;
            }
        }
        else
        {
            l_success = true;
        }

        if (l_pos2 != std::string::npos)
        {
            // Find the final const field if it exists
            l_pos1 = l_line.find_first_not_of(" \t", l_pos2);

            if (l_pos1 == std::string::npos)
            {
                break;
            }

            std::string l_constStr = l_line.substr(l_pos1);

            if (0 == l_constStr.find(ATTR_CONST))
            {
                o_const = true;
            }
        }

    }
    while (0);

    return l_success;
}



//******************************************************************************
bool attrFileTargetLineToData(
    const std::string& i_line,
    uint32_t& o_targetType,
    uint16_t& o_targetPos,
    uint8_t& o_targetUnitPos,
    uint8_t& o_targetNode)
{
    /*
     * e.g. "target = k0:n0:s0:centaur.mba:p02:c1"
     * - o_targetType = 0x00000001
     * - o_targetPos = 2
     * - o_targetUnitPos = 1
     */
    // If the target string is not decoded into a non-system target and
    // explicit positions are not found then the caller will get these defaults
    bool l_sysTarget = true;
    o_targetType     = fapi2::TARGET_TYPE_SYSTEM;
    o_targetNode     = ATTR_NODE_NA;
    o_targetPos      = ATTR_POS_NA;
    o_targetUnitPos  = ATTR_UNIT_POS_NA;

    // Find the node, target type, pos and unit-pos
    if (0 == i_line.find(ATTR_FILE_TARGET_EXT_HEADER_STR))
    {
        // Create a local string and remove the target header
        std::string l_line =
            i_line.substr(strlen(ATTR_FILE_TARGET_EXT_HEADER_STR));

        // Figure out the node number
        if (0 == l_line.find(TARGET_NODE_HEADER_STR))
        {
            l_line = l_line.substr(strlen(TARGET_NODE_HEADER_STR));

            if (0 == l_line.find(TARGET_NODE_ALL_STR))
            {
                l_line = l_line.substr(strlen(TARGET_NODE_ALL_STR));
            }
            else
            {
                o_targetNode = strtoul(l_line.c_str(), NULL, 10);
                size_t l_pos = l_line.find(':');

                if (l_pos != std::string::npos)
                {
                    l_line = l_line.substr(l_pos);
                }
                else
                {
                    l_line.clear();
                }
            }
        }

        if (0 == l_line.find(ATTR_FILE_TARGET_EXT_FOOTER_STR))
        {
            // Remove the target footer
            l_line = l_line.substr(strlen(ATTR_FILE_TARGET_EXT_FOOTER_STR));
        }

        // Figure out the target type
        //Remove the end of the target string (position and unitpos) before
        //using the line to search for target types
        auto l_pos = l_line.find(":");
        std::string l_targetType = std::string ();
        std::string l_chipType = std::string ();
        std::string l_chipUnitType = std::string ();

        TargStrToType* chip_type_last =
            &CHIP_TYPE_TARG_STR_TO_TYPE
            [sizeof(CHIP_TYPE_TARG_STR_TO_TYPE) / sizeof(TargStrToType)];

        TargStrToType* chip_unit_type_last =
            &CHIP_UNIT_TYPE_TARG_STR_TO_TYPE
            [sizeof(CHIP_UNIT_TYPE_TARG_STR_TO_TYPE) / sizeof(TargStrToType)];

        TargStrToType* item = NULL;

        if (l_pos != std::string::npos)
        {
            l_targetType = l_line.substr(0, l_pos);
            auto l_chipPos = l_targetType.find(".");

            if (l_chipPos != std::string::npos)
            {
                //"." found, meaning both chip type and chip unit are specified
                std::string l_chipUnit = l_targetType.substr(l_chipPos + 1);
                item = std::find(&CHIP_UNIT_TYPE_TARG_STR_TO_TYPE[0],
                                 chip_unit_type_last, l_chipUnit.c_str());

                if (item != chip_unit_type_last)
                {
                    o_targetType = item->iv_fapiType;
                    l_line = l_line.substr(l_targetType.length());
                    l_sysTarget = false;
                }
            }
            else
            {
                //only chip type is specified
                item = std::find(&CHIP_TYPE_TARG_STR_TO_TYPE[0], chip_type_last,
                                 l_targetType.c_str());

                if (item != chip_type_last)
                {
                    o_targetType = item->iv_fapiType;
                    l_line = l_line.substr(l_targetType.length());
                    l_sysTarget = false;
                }
            }
        }
        else
        {
            l_sysTarget = true;
        }




        // For a non-system target, figure out the position and unit position
        if (l_sysTarget == false)
        {
            // Figure out the target's position
            if (0 == l_line.find(TARGET_POS_HEADER_STR))
            {
                l_line = l_line.substr(strlen(TARGET_POS_HEADER_STR));

                if (0 == l_line.find(TARGET_POS_ALL_STR))
                {
                    l_line = l_line.substr(strlen(TARGET_POS_ALL_STR));
                }
                else
                {
                    o_targetPos = strtoul(l_line.c_str(), NULL, 10);

                    size_t l_pos = l_line.find(':');

                    if (l_pos != std::string::npos)
                    {
                        l_line = l_line.substr(l_pos);
                    }
                    else
                    {
                        l_line.clear();
                    }
                }
            }

            // Figure out the target's unit position
            if (0 == l_line.find(TARGET_UNIT_POS_HEADER_STR))
            {
                l_line = l_line.substr(strlen(TARGET_UNIT_POS_HEADER_STR));

                if (0 == l_line.find(TARGET_POS_ALL_STR))
                {
                    l_line = l_line.substr(strlen(TARGET_POS_ALL_STR));
                }
                else
                {
                    o_targetUnitPos = strtoul(l_line.c_str(), NULL, 10);
                }
            }
        }
    }
    else
    {
        std::cerr << "ERROR: Unsupported syntax for target line. Actual: " <<
                  i_line << " Expected: target = k0:n0...\n" << std::endl;
        return false;
    }

    // System targets must have an NA node
    if (l_sysTarget)
    {
        o_targetNode = ATTR_NODE_NA;
    }

    return true;

}

//******************************************************************************
bool attrFileAttrLinesToData(
    std::vector<std::string>& i_lines,
    uint32_t& o_attrId,
    uint32_t& o_valSizeBytes,
    uint8_t*& o_pVal,
    bool& o_const)
{
    bool l_success = true;
    size_t l_numElements = 0;

    // Data for the attribute
    uint32_t l_attrElemSizeBytes = 0;
    size_t d[ATTR_MAX_DIMS] = {0}; // dimensions of the attribute

    // Data for this line. Note that this function expects all lines to be for
    // the same attr (in the case of an array attribute)
    std::string l_attrString;
    size_t td[ATTR_MAX_DIMS] = {0}; // dimensions of this line's element
    std::string l_valString;
    uint64_t l_attrVal = 0;

    std::vector<std::string>::const_iterator l_itr;

    // Iterate over each line
    for (l_itr = i_lines.begin(); l_itr != i_lines.end(); ++l_itr)
    {
        // Split the attribute line into fields
        l_success = attrFileAttrLineToFields(*l_itr, l_attrString, td,
                                             l_valString, o_const);

        if (!l_success)
        {
            printf(
                "attrFileAttrLinesToData: "
                "Error. Could not break into fields (%s)\n",
                (*l_itr).c_str());
            break;
        }

        if (o_pVal == NULL)
        {

            l_success = getAttrDataFromMap(l_attrString.c_str(),
                                           o_attrId,
                                           l_attrElemSizeBytes,
                                           d);

            if(!l_success)
            {
                printf("There was a problem getting data for %s\n",
                       l_attrString.c_str());
                break;
            }

            o_valSizeBytes = l_attrElemSizeBytes * d[0] * d[1] * d[2] * d[3];
            o_pVal = new uint8_t[o_valSizeBytes];
        }

        // Check that the attribute isn't overflowing an array
        for(size_t i = 0; i < ATTR_MAX_DIMS; i++)
        {
            if(td[i] >= d[i])
            {
                printf("attrFileAttrLinesToData: Error. Array Overflow (%s)\n",
                       (*l_itr).c_str());
                break;
            }
        }

        if ((l_valString[0] == '0') && (l_valString[1] == 'x'))
        {
            // Value string is a value
            l_attrVal = strtoull(l_valString.c_str(), NULL, 0);
        }
        else
        {
            // Value string is an enumerator, it is decoded using <attr>_<enum>
            l_valString = l_attrString + "_" + l_valString;

            l_success = getAttrEnumDataFromMap(l_valString.c_str(), l_attrVal);

            if(!l_success)
            {
                printf("An error occurred when retrieving the enum value for"
                       " %s\n", l_valString.c_str());
                break;
            }

        }

        // Write the element to the correct place in the buffer
        size_t l_size = sizeof(d) / sizeof(size_t);
        size_t l_elem = 0;

        for(size_t idx = 0; idx < l_size; idx++)
        {
            l_elem *= d[idx];
            l_elem += td[idx];
        }


        if (l_attrElemSizeBytes == sizeof(uint8_t))
        {
            o_pVal[l_elem] = l_attrVal;
            l_numElements++;
        }
        else if (l_attrElemSizeBytes == sizeof(uint16_t))
        {
            uint16_t* l_pVal = reinterpret_cast<uint16_t*>(o_pVal);
            l_pVal[l_elem] = (l_attrVal);
            l_numElements++;
        }
        else if (l_attrElemSizeBytes == sizeof(uint32_t))
        {
            uint32_t* l_pVal = reinterpret_cast<uint32_t*>(o_pVal);
            l_pVal[l_elem] = (l_attrVal);
            l_numElements++;
        }
        else
        {
            uint64_t* l_pVal = reinterpret_cast<uint64_t*>(o_pVal);
            l_pVal[l_elem] = (l_attrVal);
            l_numElements++;
        }
    }

    return l_success;
}


//******************************************************************************
bool getAttrDataFromMap(const char* i_attrString,
                        uint32_t& o_attrId,
                        uint32_t& o_attrElemSizeBytes,
                        size_t (& o_dims)[4])
{
    bool l_success = true;

    do
    {
        // Check for attribute inside attribute data maps.
        const AttributeData* currentAttr = findAttribute(g_FapiAttrs,
                                           sizeof(g_FapiAttrs) / sizeof(AttributeData),
                                           i_attrString);

        if (NULL == currentAttr)
        {
            printf("Attribute data not present for the attribute %s!\n",
                   i_attrString);
            l_success = false;
            break;
        }

        o_attrId = currentAttr->iv_attrId;
        o_attrElemSizeBytes = currentAttr->iv_attrElemSizeBytes;

        for(size_t i = 0; i < sizeof(o_dims) / sizeof(size_t); ++i)
        {
            o_dims[i] = currentAttr->iv_dims[i];
        }

    }
    while( 0 );

    return l_success;
}

//******************************************************************************
bool getAttrEnumDataFromMap(const char* i_attrString,
                            uint64_t& o_enumVal)
{
    bool l_success = true;

    do
    {
        const AttributeEnum* currentAttr = NULL;
        currentAttr = findAttribute(g_FapiEnums,
                                    sizeof(g_FapiEnums) / sizeof(AttributeEnum),
                                    i_attrString);

        if (NULL == currentAttr)
        {
            printf("Could not find the ENUM value for %s\n", i_attrString);
            l_success = false;
            break;
        }

        o_enumVal = currentAttr->iv_value;

    }
    while( 0 );

    return l_success;
}

//******************************************************************************
bool attrTextToData (std::string i_inFile,
                     fapi2::TgtAttrMap_t& i_attrOvd)
{
    bool l_success = true;

    std::ifstream i_file;
    i_file.open(i_inFile);

    if (!(i_file.good()))
    {
        std::cerr << "Failed to open file: " << i_inFile << std::endl;
        return false;
    }

    // Attribute Data
    uint32_t l_attrId     = 0;
    uint32_t l_targetType = 0;
    uint32_t l_valSize    = 0;
    uint8_t* l_pVal       = NULL;
    bool l_const          = false;

    uint16_t l_pos     = ATTR_POS_NA;
    uint8_t  l_unitPos = ATTR_UNIT_POS_NA;
    uint8_t  l_node    = ATTR_NODE_NA;

    std::string l_line;
    std::string l_targetLine;
    std::string l_attrString;
    std::string l_thisAttrString;
    std::vector<std::string> l_attrLines;

    size_t l_whitespacePos;

    AttributeOvdData l_attrData;
    std::string l_attrName;

    // Iterate over all lines in the file.
    do
    {
        // Iterate over all attribute lines for the same attribute. For
        // multi-dimensional attributes, there is a line for each element
        l_attrString.clear();
        l_attrLines.clear();

        do
        {
            // Read next line.
            if (!l_line.length())
            {
                std::getline(i_file, l_line);

                if (!l_line.length())
                {
                    break;
                }
            }

            //Remove any leading whitespace
            l_whitespacePos = l_line.find_first_not_of(" \t");
            l_line = l_line.substr(l_whitespacePos, l_line.size());

            // Process the line.  Could be:
            //    * Target line.
            //    * Attribute line.
            //    * other line.
            if (attrFileIsTargLine(l_line))
            {
                if (l_attrString.empty())
                {
                    // Not currently processing attribute lines, save the target
                    // line, it is for following attribute lines
                    l_targetLine = l_line;
                    l_line.clear();
                }
                else
                {
                    // Currently processing attribute lines. Break out of the
                    // loop to process the current set and look at this target
                    // line in the next iteration
                    break;
                }
            }
            else if (attrFileIsAttrLine(l_line, l_thisAttrString))
            {
                // Found an Attribute line.
                if (l_attrString.empty())
                {
                    // First attribute of the set
                    l_attrString = l_thisAttrString;
                }
                else if (l_attrString != l_thisAttrString)
                {
                    // This attribute is different from the current set. Break
                    // out of the loop to process the current set and look at
                    // this new attribute in the next iteration
                    break;
                }

                // Add the attribute line to the vector and get the next line
                l_attrLines.push_back(l_line);
                l_line.clear();
            }
            else
            {
                // Not a target or attribute line, get the next line
                // If CLEAR line, just get next line since we arent
                // directly dealing with any tanks.
                l_line.clear();
            }
        }
        while(1);

        if (l_attrLines.size())
        {
            // Get the attribute data for this attribute
            l_success = attrFileAttrLinesToData(l_attrLines, l_attrId, l_valSize,
                                                l_pVal, l_const);

            if (!l_success)
            {
                printf("attrTextToBinaryBlob:"
                       " Error parsing attribute data\n");
                break;
            }

            // Get the Target Data for this attribute
            l_success = attrFileTargetLineToData(l_targetLine, l_targetType,
                                                 l_pos, l_unitPos, l_node);

            if (!l_success)
            {
                printf("attrTextToBinaryBlob:"
                       " Error parsing target line\n");
                break;
            }


            //Map <targetnumber, map <attributeId, data>>
            //TargetId = [Node(8-bits) Pos(16-bits) UnitPos (8-bits)]
            void* l_val = malloc(l_valSize);
            memcpy(l_val, l_pVal, l_valSize);
            uint64_t l_tgtType = l_targetType;
            uint64_t l_targetId = (l_tgtType << 32) |
                                  (l_node << 24) |
                                  (l_pos  << 8 ) |
                                  (l_unitPos);

            i_attrOvd [l_targetId][l_attrId] = l_val;

            delete[] l_pVal;
            l_pVal = NULL;
        }

    }
    while (!i_file.eof());

    i_file.close();
    return l_success;
}
}



