# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/initfiles.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2017
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG
###############################################################################
# Macros to compile the initfile into procedures
###############################################################################

_internal_empty=
_internal_space=$(_internal_empty) $(_internal_empty)
_internal_comma=,

#SINGLE_INITFILE_BUILD
#    This macro defines the commands to compile a single initfile
#        Input: initfile with it's path and whether compiling for spy or scom
#    Order of operation:
#       - Run initCompiler.exe -i <initfile_path/initfile_name> -o <output-dir>
#             --spy/scom --chipId <$3> --chipEc <$4>
#       - Copy the generated procedures from output dir to the source tree. So,
#         we don't erase the generated procedure each time we do a ekb build clean
#       - In the source tree, procedures end up at
#         /chips/$(chips)/procedures/hwp/initfiles/
define SINGLE_INITFILE_BUILD
		$(C1) $$$$< --initfile $(1) \
			--output-dir $$($(notdir $1)_PATH) --$2 \
			$(foreach str,$(3), --chip $(str))\
			&& cp -t $(dir $1)/../procedures/hwp/initfiles \
			$$($(notdir $1)_PATH)/$(subst _initfile,,$(subst .,_,$(notdir $1))).* \
			&& cd $(ROOTPATH) &&  \
			git diff --name-only --diff-filter=AM | \
			grep $(subst _initfile,,$(subst .,_,$(notdir $1))).* | \
			xargs tools/hooks/tools/pre-commit-actions > /dev/null
endef

#SINGLE_INITFILE_TARGETS
#   This macro adds generated files to the TARGET variable
define SINGLE_INITFILE_TARGETS
TARGETS += $(patsubst %_initfile,%.C,$1)
TARGETS += $(patsubst %_initfile,%.H,$1)
TARGETS += $(patsubst %_initfile,%.mk,$1)
endef

#SINGLE_INITFILE_PATHS
#    This macro set the paths for a single spy initfile
#       Defines the command to run: initCompiler.exe
#       Defines the command path: /output/bin/
#       Defines the output path: /output/gen/initfiles/
define SINGLE_INITFILE_PATHS
COMMAND=initCompiler.exe
$1_COMMAND_PATH = $(EXEPATH)/
$1_PATH = $(GENPATH)/initfiles/
endef

# -------------------------------------------------------------------------
# BUILD_INITFILE
# Building C/H files for the input initfile
# This macro generates all the recipes for building a single spy initfile
#    Input: initfile with it's path, spy/scom, chipId, chipEc, chip
#    Order of operation:
#       - Define the name of the generator: initfile_initfile_name
#       - Define the command to run to compile the initfile
#       - Define target files
#       - Define source files
#       - Define paths to the initCompiler.exe and output files
#       - call BUILD_GENERATED macro which runs the right recipes to compile
#         this file
# -------------------------------------------------------------------------
define BUILD_INITFILE
$(eval GENERATED=$(notdir $1))
$(eval $(notdir $1)_RUN=$(call SINGLE_INITFILE_BUILD,$1,$2,$3))
$(eval $(call SINGLE_INITFILE_TARGETS,$(subst .,_,$(notdir $1))))
$(eval SOURCES+=$1)
$(eval $(call SINGLE_INITFILE_PATHS,$(GENERATED)))
$(call BUILD_GENERATED)
endef

# -------------------------------------------------------------------------
# GET_CHIP_SCAN_INITFILES
# Get the SCAN initfiles associated with the input chip (p9 or centaur)
# -------------------------------------------------------------------------
define GET_CHIP_SCAN_INITFILES
$(eval SCAN_INITFILES_DIR=$(addsuffix /initfiles/*.scan.initfile,$(addprefix $(ROOTPATH)/chips/,$(1))))
$(eval SCAN_INITFILES=$(wildcard $(SCAN_INITFILES_DIR)))
endef

# -------------------------------------------------------------------------
# BUILD_ALL_SCAN_INITFILES
# Process chip initfile
# Pseudo 
#
#    CHIPS         : p9 centaur
#    CHIP_EC_PAIRS : p9:p9n:10 p9:p9n:20 p9:p9c:10 centaur:cen:20
#    CHIPID        : p9n p9c cen
#
#    for each chip in CHIPS
#        if chip exists in CHIP_EC_PAIRS, add initfile from chip initfile dir (chips/<chip>/initfiles/) to SCAN_INITFILES
#        for each initfile in SCAN_INITFILES
#            if initfile contains '$(chip).' ('p9.' or 'centaur.'), call BUILD_INITFILE, passing in all $(chip)% in CHIP_EC_PAIRS
#                               (example: p9.xxx.initfile --> pass in 'p9:p9n:10 p9:p9n:20 p9:p9c:10'
#                                         centaur.xxx.initfile --> pass in 'centaur:cen:20')
#            for each id in CHIPID
#                if initfile contains '$(id).' ('p9n.', 'p9c.', 'cen.'), call BUILD_INITFILE, passing in all $(chip):$(id)% in CHIP_EC_PAIRS
#                               (example: p9n.xxx.initfile --> pass in 'p9:p9n:10 p9:p9n:20'
#                                         p9c.xxx.initfile --> pass in 'p9:p9c:10')
#
#
# -------------------------------------------------------------------------
define BUILD_ALL_SCAN_INITFILES
$(foreach chip, $(CHIPS),\
	$(if $(findstring $(chip), $(CHIP_EC_PAIRS)),$(call GET_CHIP_SCAN_INITFILES, $(chip)) \
	$(foreach initfile,$(SCAN_INITFILES),\
		$(if $(findstring $(addsuffix .,$(chip)), $(initfile)),$(eval $(call BUILD_INITFILE,$(initfile),spy,\
			$(filter $(chip)%, $(CHIP_EC_PAIRS))))) \
		$(foreach id,$($(chip)_CHIPID),\
			$(if $(findstring $(addsuffix .,$(id)), $(initfile)),$(eval $(call BUILD_INITFILE,$(initfile),spy,\
				$(filter $(chip):$(id)%, $(CHIP_EC_PAIRS)))))))))

endef

# -------------------------------------------------------------------------
# GET_CHIP_SCOM_INITFILES
# Get the SCOM initfiles associated with the input chip (p9 or centaur)
# -------------------------------------------------------------------------
define GET_CHIP_SCOM_INITFILES
$(eval SCOM_INITFILES_DIR=$(addsuffix /initfiles/*.scom.initfile,$(addprefix $(ROOTPATH)/chips/,$(1))))
$(eval SCOM_INITFILES=$(wildcard $(SCOM_INITFILES_DIR)))
endef

# -------------------------------------------------------------------------
# BUILD_ALL_SCOM_INITFILES
# Run all the .scom.initfile through the initCompiler
#
# Pseudo 
#
#    CHIPS         : p9 centaur
#    CHIP_EC_PAIRS : p9:p9n:10 p9:p9n:20 p9:p9c:10 centaur:cen:20
#    CHIPID        : p9n p9c cen
#
#    for each chip in CHIPS
#        if chip exists in CHIP_EC_PAIRS, add initfile from chip initfile dir (chips/<chip>/initfiles/) to SCOM_INITFILES
#        for each initfile in SCOM_INITFILES
#            if initfile contains '$(chip).' ('p9.' or 'centaur.'), call BUILD_INITFILE, passing in all $(chip)% in CHIP_EC_PAIRS
#                               (example: p9.xxx.initfile --> pass in 'p9:p9n:10 p9:p9n:20 p9:p9c:10'
#                                         centaur.xxx.initfile --> pass in 'centaur:cen:20')
#            for each id in CHIPID
#                if initfile contains '$(id).' ('p9n.', 'p9c.', 'cen.'), call BUILD_INITFILE, passing in all $(chip):$(id)% in CHIP_EC_PAIRS
#                               (example: p9n.xxx.initfile --> pass in 'p9:p9n:10 p9:p9n:20'
#                                         p9c.xxx.initfile --> pass in 'p9:p9c:10')
# -------------------------------------------------------------------------
define BUILD_ALL_SCOM_INITFILES
$(foreach chip, $(CHIPS),\
	$(if $(findstring $(chip), $(CHIP_EC_PAIRS)),$(call GET_CHIP_SCOM_INITFILES, $(chip)) \
	$(foreach initfile,$(SCOM_INITFILES),\
		$(if $(findstring $(addsuffix .,$(chip)), $(initfile)),$(eval $(call BUILD_INITFILE,$(initfile),scom,\
			$(filter $(chip)%, $(CHIP_EC_PAIRS))))) \
		$(foreach id,$($(chip)_CHIPID),\
			$(if $(findstring $(addsuffix .,$(id)), $(initfile)),$(eval $(call BUILD_INITFILE,$(initfile),scom,\
				$(filter $(chip):$(id)%, $(CHIP_EC_PAIRS)))))))))

endef

#Check to see if the engd is populated for a given EC level. Create a list of
#all the chip:chipId:ec strings for all the chip/ec that we have engd for.
# $1 == chip (p9/centaur)
# $2 == chipId (p9n/p9c)
# $3 == ec (10/11)
define CHECK_ENGD_EXISTS
$(eval CHIP_EC_PAIRS+=$(if $(wildcard $(ROOTPATH)/chips/$1/engd/$2/$3/*),$1:$2:$3))
endef


#Builds all the initfiles
#Order of operation:
#    - Check to see if engd exists for all the supported ec levels
#    - If we have valid chip/ec pairs based on the existence of engd, then
#         - build initCompiler
#         - build scan initfiles
#         - build scom initfiles
define BUILD_ALL_INITFILES
$(foreach chip,$(CHIPS),\
	$(foreach id,$($(chip)_CHIPID),\
	$(foreach ec,$($(id)_EC),\
	$(eval $(call CHECK_ENGD_EXISTS,$(chip),$(id),$(ec))))))
$(if $(strip $(CHIP_EC_PAIRS)),$(eval $(call BUILD_INITCOMPILER)))
$(if $(strip $(CHIP_EC_PAIRS)),$(eval $(call BUILD_ALL_SCAN_INITFILES,$(CHIP_EC_PAIRS))))
$(if $(strip $(CHIP_EC_PAIRS)),$(eval $(call BUILD_ALL_SCOM_INITFILES,$(CHIP_EC_PAIRS))))
endef

$(eval $(call BUILD_ALL_INITFILES))
