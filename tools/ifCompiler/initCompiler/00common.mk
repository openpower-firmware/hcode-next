# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/initCompiler/00common.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2016,2017
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

#Create symlinks for all the fapi2 files that are needed by fapi2_ifCompiler

FILES_TO_SYMLINK = fapi2_attribute_service.C
FILES_TO_SYMLINK += attribute_ids.H
FILES_TO_SYMLINK += hwp_return_codes.H
FILES_TO_SYMLINK += fapi2_chip_ec_feature.H
FILES_TO_SYMLINK += hwp_ffdc_classes.H
FILES_TO_SYMLINK += hwp_error_info.H
FILES_TO_SYMLINK += set_sbe_error.H
FILES_TO_SYMLINK += set_sbe_error_funcs.H
FILES_TO_SYMLINK += fapi2AttrOverrideData.H
FILES_TO_SYMLINK += fapi2AttrOverrideEnums.H

FAPI2_GEN_FILES  = $(addprefix $(GENPATH)/,$(FILES_TO_SYMLINK))
$(foreach file,$(FAPI2_GEN_FILES),\
	$(eval $(call CREATE_SYMLINK,$(file),$(GENPATH)/fapi2_ifCompiler)))
