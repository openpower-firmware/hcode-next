/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/initCompiler/ScanFileEvaluator.C $           */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2017                                                         */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
#include <ScanFileEvaluator.H>

namespace init_compiler
{

int ScanFileEvaluator::processSpys(std::vector <spy*>& i_spys,
                                   ChipInfoMap_t& i_ecMap)
{
    // process each spy in the file individually for scan
    for (auto* l_spy : i_spys)
    {
        auto supportedChipEcs = l_spy->getSupportedChipEcLevels();

        // skip it if not supported for any listed chip/ECs
        if( supportedChipEcs->size() > 0)
        {
            if(l_spy->areAllEcsSupported())
            {
                processSpyForAllChipEcs(l_spy, *supportedChipEcs);
            }
            else
            {
                // if we are going to create a path based on Chip/ECs print out
                // the locals here, outside the first conditional so they
                // are available to each case (think attributes)
                printRequiredVariables(l_spy);

                // create spy level branch based on chip/Ec level
                SpyLevelChipEcConditional(l_spy, *supportedChipEcs);
            }
        }
    }

    return 0;
}
int ScanFileEvaluator::processRows(BitRange_t i_bits, Row*& i_row,
                                   spy*& i_spy, uint8_t i_chipId,
                                   uint32_t i_chipEc)
{
    int l_rc = (i_spy->isIspy()) ?
               processIspyRows(i_bits, i_row, i_spy, i_chipId, i_chipEc) :
               processEspyRows(i_row, i_spy, i_chipId, i_chipEc);

    return l_rc;
}

}
