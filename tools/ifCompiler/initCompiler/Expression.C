/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/initCompiler/Expression.C $                  */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2016                                                         */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
#include <Expression.H>
#include <initCompiler_common.H>

using namespace init_compiler;

/*********** Expression Class ********************/
Expression::Expression(Node* i_lhs, Node* i_rhs,
                       const std::string& i_operator)
    : iv_left_hand_side(i_lhs),
      iv_right_hand_side(i_rhs),
      iv_operator(i_operator)
{
}

Expression::~Expression(void)
{
    delete iv_left_hand_side;
    delete iv_right_hand_side;
}

std::string Expression::toSymbol(void)
{
    return ("(" + iv_left_hand_side->toSymbol() + " " + iv_operator + " " +
            iv_right_hand_side->toSymbol() + ")");
}

std::string Expression::toString(void)
{
    return ("(" + iv_left_hand_side->toString() + " " + iv_operator + " " +
            iv_right_hand_side->toString() + ")");
}


