/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/initCompiler/Node.H $                        */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2016                                                         */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
#ifndef _NODE_H
#define _NODE_H

#include <chipEngData.H>
#include <map>
#include <set>
#include <unordered_set>
#include <iostream>
#include <initCompiler_common.H>

namespace init_compiler
{
/*
 * @brief: Parent class for expression, define, literal, enums, and
 *         attributes. Also, used for empty spots (true statements, etc.)
 */
class Node
{
    public:
        /*
         * @brief: Construct an empty Node
         */
        Node (void) {}

        /*
         * @brief: Destructor - nothing to do
         */
        virtual ~Node (void) {}

        /*
         * @brief: Returns original name of Node
         */
        virtual std::string toString(void)
        {
            return std::string();
        }

        /*
         * @brief: Returns converted variable name (for attributes,
         *         literals, and defines)
         */
        virtual std::string toSymbol(void)
        {
            return std::string();
        }

        /*
         * @brief: get the node on the left side of an expression
         * @retval returns a pointer to the Node object
         */
        virtual Node* getLHS ()
        {
            return new Node();
        }

        /*
         * @brief: get the node on the right side of an expression
         * @retval: returns a pointer to the Node object
         */
        virtual Node* getRHS ()
        {
            return new Node();
        }

        /*
         * @brief: get the operator
         * @retval: returns a string containing the operator
         */
        virtual std::string getOperator ()
        {
            return std::string();
        }

        /*
         * @brief: sets isEspy to true in Literal class
         */
        virtual void setIsEspy (void) {}

};
};

#endif
