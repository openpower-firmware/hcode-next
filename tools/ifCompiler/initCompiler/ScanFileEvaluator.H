/* IBM_PROLOG_BEGIN_TAG                                                   */
/* This is an automatically generated prolog.                             */
/*                                                                        */
/* $Source: tools/ifCompiler/initCompiler/ScanFileEvaluator.H $           */
/*                                                                        */
/* IBM CONFIDENTIAL                                                       */
/*                                                                        */
/* EKB Project                                                            */
/*                                                                        */
/* COPYRIGHT 2017                                                         */
/* [+] International Business Machines Corp.                              */
/*                                                                        */
/*                                                                        */
/* The source code for this program is not published or otherwise         */
/* divested of its trade secrets, irrespective of what has been           */
/* deposited with the U.S. Copyright Office.                              */
/*                                                                        */
/* IBM_PROLOG_END_TAG                                                     */
#ifndef __SCANFILE_EVALUATOR_H
#define __SCANFILE_EVALUATOR_H

#include <Evaluator.H>
namespace init_compiler
{
/*
 * @brief: Evaluator class to process scan initfiles and generate hardware
 *         procedure code to be run by firmware
 */
class ScanFileEvaluator : public Evaluator
{
    public:
        /*
         * @brief: ScomFileEvaluator constructor
         */
        ScanFileEvaluator():
            Evaluator()
        {
        }

        /*
         * @brief: ScomFileEvaluator destructor
         */
        ~ScanFileEvaluator() {}

        /*
         * @brief: main entry point into the class, called buy the base class
         *         this is where the actual initfile spy definitions are
         *         processed
         */
        int processSpys(std::vector <spy*>& i_spys,
                        ChipInfoMap_t& i_ecMap);

        /*
         * @brief: called buy the base class this is where the
         *         actual initfile rows are processed
         */
        int processRows(BitRange_t i_bits, Row*& i_row,
                        spy*& i_spy, uint8_t i_chipId,
                        uint32_t i_chipEc);

};
} // namespace
#endif
