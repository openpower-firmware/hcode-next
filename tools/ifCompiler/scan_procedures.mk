# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/scan_procedures.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2019
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

export TOPLEVEL=$(ROOTPATH)

###############################################################################
# Ring output directories
###############################################################################
# folder name containing rings for SBE
export IPL_BASE=ipl_base
export IPL_RISK=ipl_risk
export IPL_CACHE_CONTAINED=ipl_cache_contained

# folder name containg rings for CME/SGPE
export RUNTIME_BASE=runtime_base
export RUNTIME_RISK=runtime_risk
export RUNTIME_RISK2=runtime_risk2
export RUNTIME_RISK3=runtime_risk3
export RUNTIME_RISK4=runtime_risk4
export RUNTIME_RISK5=runtime_risk5

#stump rings will always end up in the runtime_base/marker directory
export STUMP_PHASE=marker

QUAD_PHASES = $(IPL_BASE) $(IPL_CACHE_CONTAINED) $(RUNTIME_BASE) $(RUNTIME_RISK) $(RUNTIME_RISK2) $(RUNTIME_RISK3) $(RUNTIME_RISK4) $(RUNTIME_RISK5)
QUAD_RUNTIME_PHASES = $(RUNTIME_BASE) $(RUNTIME_RISK) $(RUNTIME_RISK2) $(RUNTIME_RISK3) $(RUNTIME_RISK4) $(RUNTIME_RISK5)

###############################################################################
# ADD_SCAN_INPUT_RULE
#     Add a single entry to the SCAN_INPUTS list
#
#     Input:
#       $1 == phase/output folder name
#       $2 == initfile name
#       $3 == space separated list of targets required to process initfile
#       $4 == chip (p9 or centaur)
#       $5 == ec
#
# Syntax for SCAN_INPUTS is:
# <name of the run>|<initfile>|<comma sep list of targets>|<attr override file>
###############################################################################
define ADD_SCAN_INPUT_RULE
$(eval targets = $(subst $(_internal_space),$(_internal_comma),$(3)))
$(eval $4_$5_SCAN_INPUTS +=$(1)|$(2)|k0:n0:s0:$(targets)|$(1).txt|$(6))
endef

###############################################################################
# ADD_PLL_SCAN_INPUT_RULE
#     Add a single entry to the SCAN_INPUTS list
#
#     Input:
#       $1 == initfile name
#       $2 == space separated list of targets required to process initfile
#       $3 == bucket type (nest/mem/clk)
#       $4 == bucket number
#       $5 == chip (p9 or centaur)
#       $6 == ec
#
# Syntax for SCAN_INPUTS is:
# <name of the run>|<initfile>|<comma sep list of targets>|<attr override file>
###############################################################################
define ADD_PLL_SCAN_INPUT_RULE
$(eval targets = $(subst $(_internal_space),$(_internal_comma),$(2)))
$(eval $5_$6_SCAN_INPUTS += $(IPL_BASE)|$(1)|k0:n0:s0:$(targets)|$(3)_bucket$(4).txt|bucket_$(4))
endef

###############################################################################
# ADD_PHASE_CLK_SCAN_INPUT_RULE
#     Add a single entry to the SCAN_INPUTS list
#
#     Input:
#       $1 == phase
#       $2 == initfile name
#       $3 == space separated list of targets required to process initfile
#       $4 == bucket type (nest/mem/clk)
#       $5 == bucket number
#
# Syntax for SCAN_INPUTS is:
# <name of the run>|<initfile>|<comma sep list of targets>|<attr override file>
###############################################################################
define ADD_PHASE_CLK_SCAN_INPUT_RULE
$(eval targets = $(subst $(_internal_space),$(_internal_comma),$(3)))
$(eval $6_$7_SCAN_INPUTS += $(1)|$(2)|k0:n0:s0:$(targets)|$(4)_bucket$(5).txt|bucket_$(5))
endef

###############################################################################
# BUILD_QUAD_INITFILE
#    This macro iterates over all build phases required for a given quad
#    initfile and adds an entry to SCAN_INPUTS for each phase
#
#    This macro is to be used on p9.xxx.initfile (common Nimbus/Cumulus
#                                                 QUAD initfiles)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
###############################################################################
define BUILD_QUAD_INITFILE
$(foreach chip_ec_pair,$(filter p9%,$(CHIP_EC_PAIRS)),\
        $(foreach phase, $(QUAD_PHASES),\
                 $(eval $(call ADD_SCAN_INPUT_RULE,$(phase),$(1),$(2),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair)))))))
endef

###############################################################################
# BUILD_QUAD_NIMBUS_INITFILE
#    This macro iterates over all build phases required for a given quad
#    initfile and adds an entry to SCAN_INPUTS for each phase
#
#    This macro is to be used on p9n.xxx.initfile (Nimbus QUAD initfiles only)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
###############################################################################
define BUILD_QUAD_NIMBUS_INITFILE
$(foreach chip_ec_pair,$(filter p9:p9n%,$(CHIP_EC_PAIRS)),\
        $(foreach phase, $(QUAD_PHASES),\
                 $(eval $(call ADD_SCAN_INPUT_RULE,$(phase),$(1),$(2),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair)))))))
endef

###############################################################################
# BUILD_NEST_INITFILE
#    This macro iterates over all build phases required for a given nest
#    (non-quad) initfile and adds an entry to SCAN_INPUTS for each phase
#
#    This macro is to be used on p9.xxx.initfile (common Nimbus/Cumulus
#                                                 NEST initfiles)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
###############################################################################
NEST_PHASES = $(IPL_BASE) $(IPL_RISK)

define BUILD_NEST_INITFILE
$(foreach chip_ec_pair,$(filter p9%,$(CHIP_EC_PAIRS)),\
        $(foreach phase, $(NEST_PHASES),\
                 $(eval $(call ADD_SCAN_INPUT_RULE,$(phase),$(1),$(2),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair)))))))
endef

###############################################################################
# BUILD_NEST_NIMBUS_INITFILE
#    This macro iterates over all build phases required for a given nest
#    (non-quad) initfile and adds an entry to SCAN_INPUTS for each phase
#
#    This macro is to be used on p9n.xxx.initfile (Nimbus NEST initfiles only)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
###############################################################################
NEST_PHASES = $(IPL_BASE) $(IPL_RISK)

define BUILD_NEST_NIMBUS_INITFILE
$(foreach chip_ec_pair,$(filter p9:p9n%,$(CHIP_EC_PAIRS)),\
        $(foreach phase, $(NEST_PHASES),\
                 $(eval $(call ADD_SCAN_INPUT_RULE,$(phase),$(1),$(2),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair)))))))
endef

###############################################################################
# BUILD_NEST_CUMULUS_INITFILE
#    This macro iterates over all build phases required for a given nest
#    (non-quad) initfile and adds an entry to SCAN_INPUTS for each phase
#
#    This macro is to be used for Cumulus initfiles only (p9c.xxx.initfile)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
###############################################################################
NEST_PHASES = $(IPL_BASE) $(IPL_RISK)

define BUILD_NEST_CUMULUS_INITFILE
$(foreach chip_ec_pair,$(filter p9:p9c%,$(CHIP_EC_PAIRS)),\
        $(foreach phase, $(NEST_PHASES),\
                 $(eval $(call ADD_SCAN_INPUT_RULE,$(phase),$(1),$(2),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair)))))))
endef

###############################################################################
# BUILD_STUMP_INITFILE
#    This macro iterates over all build phases required for for the stumped
#    CMSK ring (currently runtime_base only)
#
#    Input:
#      $1 == initfile name
#      $2 == targets
###############################################################################
STUMP_PHASES = $(STUMP_PHASE)
define BUILD_STUMP_INITFILE
$(foreach chip_ec_pair,$(filter p9%,$(CHIP_EC_PAIRS)),\
        $(foreach phase, $(STUMP_PHASES),$(eval $(call ADD_SCAN_INPUT_RULE,$(phase),$(1),$(2),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair)))))))
endef


###############################################################################
# BUILD_PLL_INITFILE
#    This macro iterates over all operating frequency points for a given
#    initfile (operating point defined by bucket type/number) and adds an
#    entry to SCAN_INPUTS for each phase
#
#    This macro is to be used on p9.xxx.initfile (common Nimbus/Cumulus
#                                                 PLL initfiles)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
#      $3 == bucket type (nest/mem/clk/l2)
#      $4 == build reduced number of buckets (3, instead of 5)
###############################################################################
BUCKETS_5 = 1 2 3 4 5
BUCKETS_3 = 1 2 3

define BUILD_PLL_INITFILE
$(if $(4),\
	$(foreach chip_ec_pair,$(filter p9%,$(CHIP_EC_PAIRS)),\
		$(foreach bucket, $(BUCKETS_3), $(eval $(call ADD_PLL_SCAN_INPUT_RULE,$(1),$(2),$(3),$(bucket),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))), \
	$(foreach chip_ec_pair,$(filter p9%,$(CHIP_EC_PAIRS)),\
		$(foreach bucket, $(BUCKETS_5), $(eval $(call ADD_PLL_SCAN_INPUT_RULE,$(1),$(2),$(3),$(bucket),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))))
endef


###############################################################################
# BUILD_PLL_NIMBUS_INITFILE
#    This macro iterates over all operating frequency points for a given
#    initfile (operating point defined by bucket type/number) and adds an
#    entry to SCAN_INPUTS for each phase
#
#    This macro is to be used on p9n.xxx.initfile (Nimbus PLL initfiles only)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
#      $3 == bucket type (nest/mem/clk/l2)
#      $4 == build reduced number of buckets (3, instead of 5)
###############################################################################
define BUILD_PLL_NIMBUS_INITFILE
$(if $(4),\
	$(foreach chip_ec_pair,$(filter p9:p9n%,$(CHIP_EC_PAIRS)),\
        $(foreach bucket, $(BUCKETS_3), $(eval $(call ADD_PLL_SCAN_INPUT_RULE,$(1),$(2),$(3),$(bucket),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))), \
        $(foreach chip_ec_pair,$(filter p9:p9n%,$(CHIP_EC_PAIRS)),\
                $(foreach bucket, $(BUCKETS_5), $(eval $(call ADD_PLL_SCAN_INPUT_RULE,$(1),$(2),$(3),$(bucket),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))))
endef

###############################################################################
# BUILD_PLL_CUMULUS_INITFILE
#    This macro iterates over all operating frequency points for a given
#    initfile (operating point defined by bucket type/number) and adds an
#    entry to SCAN_INPUTS for each phase
#
#    This macro is to be used on p9c.xxx.initfile (Cumulus PLL initfiles only)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
#      $3 == bucket type (nest/mem/clk/l2)
#      $4 == build reduced number of buckets (3, instead of 5)
###############################################################################
define BUILD_PLL_CUMULUS_INITFILE
$(if $(4),\
	$(foreach chip_ec_pair,$(filter p9:p9c%,$(CHIP_EC_PAIRS)),\
        $(foreach bucket, $(BUCKETS_3), $(eval $(call ADD_PLL_SCAN_INPUT_RULE,$(1),$(2),$(3),$(bucket),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))), \
        $(foreach chip_ec_pair,$(filter p9:p9c%,$(CHIP_EC_PAIRS)),\
                $(foreach bucket, $(BUCKETS_5), $(eval $(call ADD_PLL_SCAN_INPUT_RULE,$(1),$(2),$(3),$(bucket),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))))
endef
###############################################################################
# BUILD_PLL_AXONE_INITFILE
#    This macro iterates over all operating frequency points for a given
#    initfile (operating point defined by bucket type/number) and adds an
#    entry to SCAN_INPUTS for each phase
#
#    This macro is to be used on p9a.xxx.initfile (Axone PLL initfiles only)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
#      $3 == bucket type (nest/mem/clk/l2/omi) 
#      $4 == build reduced number of buckets (3, instead of 5)
###############################################################################
define BUILD_PLL_AXONE_INITFILE
$(if $(4),\
	$(foreach chip_ec_pair,$(filter p9:p9a%,$(CHIP_EC_PAIRS)),\
        $(foreach bucket, $(BUCKETS_3), $(eval $(call ADD_PLL_SCAN_INPUT_RULE,$(1),$(2),$(3),$(bucket),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))), \
        $(foreach chip_ec_pair,$(filter p9:p9a%,$(CHIP_EC_PAIRS)),\
                $(foreach bucket, $(BUCKETS_5), $(eval $(call ADD_PLL_SCAN_INPUT_RULE,$(1),$(2),$(3),$(bucket),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))))
endef


###############################################################################
# BUILD_CLK_INITFILE
#
#  INPUT:
#      $1 == phase (runtime_base,ipl_base etc)
#      $2 == initfile name
#      $3 == space separated list of targets required to process initfile
#      $4 == bucket type (clk)
#      $5 == List of stesp
#      $6 == chip_ec_list (p9:p9n:10)
###############################################################################
STEPS_dcadso_dd1 =  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15
STEPS_dcadso_dd2 =  0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15

STEPS_skew6w_dd1 = 16 17 18 19 20 21 22 23 24 25
STEPS_skew6w_dd2 = 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39


define BUILD_CLK_INITFILE
$(eval CHIP= $(word 2,$(subst :, ,$(6))))
$(eval EC=$(word 3,$(subst :, ,$(6))))
$(foreach step, $(5), $(eval $(call ADD_PHASE_CLK_SCAN_INPUT_RULE,$(1),$(2),$(3),$(4),$(step),$(CHIP),$(EC))))
endef


###############################################################################
# BUILD_QUADCLK_INITFILE
#
#    This macro is to be used on p9.xxx.initfile (common Nimbus/Cumulus
#                                                 QUADCLK initfiles)
#
#  INPUT:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
#      $3 == bucket type (clk)
#      $4 == steps for nimbus dd1
#      $5 == steps for others p9c:10, p9n:20
#
#      NOTE: nimbus dd10 is a special case for these initfiles
###############################################################################
define BUILD_QUADCLK_INITFILE
$(foreach chip_ec_pair,$(filter p9%,$(CHIP_EC_PAIRS)),\
	$(foreach phase, $(QUAD_PHASES),\
		$(eval $(if $(findstring p9n:10,$(chip_ec_pair)),\
			$(call BUILD_CLK_INITFILE,$(phase),$(1),$(2),$(3),$(4),$(chip_ec_pair)),\
			$(call BUILD_CLK_INITFILE,$(phase),$(1),$(2),$(3),$(5),$(chip_ec_pair))))))
endef



###############################################################################
# BUILD_QUADCLK_NIMBUS_INITFILE
#
#    This macro is to be used on p9n.xxx.initfile (Nimbus QUADCLK initfiles only)
#
#  INPUT:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
#      $3 == bucket type (clk)
#      $4 == step number
###############################################################################
define BUILD_QUADCLK_NIMBUS_INITFILE
$(foreach chip_ec_pair,$(filter p9:p9n%,$(CHIP_EC_PAIRS)),\
	$(foreach phase, $(QUAD_PHASES),\
		$(eval $(call BUILD_CLK_INITFILE,$(phase),$(1),$(2),$(3),$(4),\
		$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair)))))))
endef

###############################################################################
# BUILD_OTHER_PLL_INITFILE
#    This macro iterates over all operating frequency points for a given
#    initfile:
#      - IPL phase only
#
#    This macro is to be used on p9.xxx.initfile (common Nimbus/Cumulus
#                                                 OTHER_PLL initfiles)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
###############################################################################
define BUILD_OTHER_PLL_INITFILE
$(foreach chip_ec_pair,$(filter p9%,$(CHIP_EC_PAIRS)),\
        $(eval $(call ADD_SCAN_INPUT_RULE,$(IPL_BASE),$(1),$(2),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))
endef
###############################################################################
# process initfiles which generate stump files for CMSK rings
###############################################################################
$(eval $(call BUILD_STUMP_INITFILE,p9.ec_func_marker.scan.initfile,pu.c k0 k0:n0:s0:pu ))


###############################################################################
# BUILD_OTHER_NIMBUS_PLL_INITFILE
#    This macro iterates over all operating frequency points for a given
#    initfile:
#      - IPL phase only
#
#    This macro is to be used on p9n.xxx.initfile (Nimbus OTHER PLL initfiles only)
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
###############################################################################
define BUILD_OTHER_NIMBUS_PLL_INITFILE
$(foreach chip_ec_pair,$(filter p9:p9n%,$(CHIP_EC_PAIRS)),\
        $(eval $(call ADD_SCAN_INPUT_RULE,$(IPL_BASE),$(1),$(2),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))
endef

###############################################################################
# BUILD_PBIEQ_INITFILE
#    This macro iterates over all supported PBIEQ configurations for a given
#    initfile (operating point defined by bucket type/number) and adds an
#    entry to SCAN_INPUTS for each phase
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
###############################################################################
PBIEQ_BUCKETS = 1 2 3 4

define BUILD_PBIEQ_INITFILE
$(foreach chip_ec_pair,$(filter p9%,$(CHIP_EC_PAIRS)),\
	$(foreach phase, $(QUAD_RUNTIME_PHASES),\
		$(foreach bucket, $(PBIEQ_BUCKETS),\
			$(eval $(call ADD_PHASE_CLK_SCAN_INPUT_RULE,$(phase),$(1),$(2),pbieq,$(bucket),\
			$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))))
endef

###############################################################################
# ADD_FILTER_PLL_SCAN_INPUT_RULE
#     Add a single entry to the SCAN_INPUTS list
#
#     Input:
#       $1 == initfile name
#       $2 == space separated list of targets required to process initfile
#       $3 == bucket type (nest/mem/clk)
#       $4 == bucket number
#       $5 == chip (p9 or centaur)
#       $6 == ec
#
# Syntax for SCAN_INPUTS is:
# <name of the run>|<initfile>|<comma sep list of targets>|<attr override file>
###############################################################################
define ADD_FILTER_PLL_SCAN_INPUT_RULE
$(eval targets = $(subst $(_internal_space),$(_internal_comma),$(2)))
$(eval $5_$6_SCAN_INPUTS += $(IPL_BASE)|$(1)|k0:n0:s0:$(targets)|$(3)_bucket$(4).txt|flt_$(4))
endef

###############################################################################
# BUILD_FILTER_PLL_INITFILE
#    This macro iterates over all operating frequency points for a given
#    initfile (operating point defined by bucket type/number) and adds an
#    entry to SCAN_INPUTS for each phase
#
#    This macro is to be used on p9.filter.pll.override.initfile only
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
#      $3 == bucket type (nest/mem/clk/l2)
#      $4 == bucket number
###############################################################################
FILTER_PLL_BUCKETS = 1 2 3 4

define BUILD_FILTER_PLL_INITFILE
$(foreach chip_ec_pair,$(filter p9%,$(CHIP_EC_PAIRS)),\
		$(foreach bucket, $(FILTER_PLL_BUCKETS), \
		$(eval $(call ADD_FILTER_PLL_SCAN_INPUT_RULE,$(1),$(2),filter_pll,$(bucket),\
		$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair)))))))
endef


###############################################################################
# process initfiles which initialize rings in the quad for:
# - SBE (HBI/cache-contained)
# - CME/SGPE (runtime)
###############################################################################
$(eval $(call BUILD_QUAD_INITFILE,p9.cme.scan.initfile,pu.eq k0 k0:n0:s0:pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.core.scan.initfile,pu.c k0 k0:n0:s0:pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.core.common.scan.initfile,pu.ex k0 k0:n0:s0:pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.l2.scan.initfile,pu.ex k0 k0:n0:s0:pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.l3.scan.initfile,pu.ex k0 k0:n0:s0:pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.ncu.scan.initfile,pu.ex k0 k0:n0:s0:pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.dpll.scan.initfile,pu.eq k0 k0:n0:s0:pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.eq.analog.scan.initfile,pu.eq k0 k0:n0:s0:pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.core.trace.scan.initfile,pu.c k0))
$(eval $(call BUILD_QUAD_INITFILE,p9.cache.trace.scan.initfile,pu.eq k0))
$(eval $(call BUILD_QUAD_INITFILE,p9.core.gptr.scan.initfile,pu.c k0 k0:n0:s0:pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.eq.gptr.scan.initfile,pu.eq k0 k0:n0:s0:pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.ex.gptr.scan.initfile,pu.ex k0 k0:n0:s0:pu))

###############################################################################
# process initfiles which initialize rings in the quad for:
# - CME/SGPE (runtime)
###############################################################################
$(eval $(call BUILD_PBIEQ_INITFILE,p9.fbc.pbieq.scan.initfile,p9n.eq k0 k0:n0:s0:pu))

###############################################################################
# Process initfiles which initialize NEST rings for:
# - SBE (HBI/cache-contained)
###############################################################################
$(eval $(call BUILD_NEST_INITFILE,p9.nx.scan.initfile,pu k0))
$(eval $(call BUILD_NEST_INITFILE,p9.npu.scan.initfile,pu))
$(eval $(call BUILD_NEST_INITFILE,p9.int.scan.initfile,pu))
$(eval $(call BUILD_NEST_INITFILE,p9.pci.scan.initfile,pu k0))
$(eval $(call BUILD_NEST_NIMBUS_INITFILE,p9n.mc.scan.initfile,pu))
$(eval $(call BUILD_NEST_CUMULUS_INITFILE,p9c.mc.scan.initfile,pu))
$(eval $(call BUILD_NEST_NIMBUS_INITFILE,p9n.mc.trace.scan.initfile,pu.mcbist k0))
$(eval $(call BUILD_NEST_INITFILE,p9.trace.scan.initfile,pu k0))
$(eval $(call BUILD_NEST_INITFILE,p9.fbc.scan.initfile,pu))
$(eval $(call BUILD_NEST_INITFILE,p9.occ.scan.initfile,pu))
$(eval $(call BUILD_QUAD_INITFILE,p9.chip.gptr.scan.initfile,pu k0))
$(eval $(call BUILD_QUAD_NIMBUS_INITFILE,p9n.mc.gptr.scan.initfile,pu.mcbist k0 k0:n0:s0:pu))
$(eval $(call BUILD_NEST_INITFILE,p9.obus.scan.initfile,pu k0))
$(eval $(call BUILD_NEST_INITFILE,p9.cxa.scan.initfile,pu))
$(eval $(call BUILD_NEST_INITFILE,p9.mmu.scan.initfile,pu k0))

###############################################################################
# process initfiles which initialize PLL rings (with buckets)
###############################################################################
$(eval $(call BUILD_PLL_NIMBUS_INITFILE,p9n.mem.pll.scan.initfile,pu.mcbist k0,mem))
$(eval $(call BUILD_PLL_CUMULUS_INITFILE,p9c.dmi.pll.scan.initfile,pu.mc k0,nest))
$(eval $(call BUILD_PLL_AXONE_INITFILE,p9a.omi.pll.scan.initfile,pu.mc k0:n0:s0:pu k0,omi))
$(eval $(call BUILD_PLL_INITFILE,p9.filter.pll.scan.initfile,pu k0,nest))
$(eval $(call BUILD_PLL_INITFILE,p9.nest.pll.scan.initfile,pu k0,nest))
$(eval $(call BUILD_PLL_INITFILE,p9.obus.pll.scan.initfile,pu.obus k0 k0:n0:s0:pu,obus,1))

###############################################################################
# process other pll initfiles (no buckets)
###############################################################################
$(eval $(call BUILD_OTHER_PLL_INITFILE,p9.pci.pll.scan.initfile,pu k0))
$(eval $(call BUILD_OTHER_PLL_INITFILE,p9.xbus.pll.scan.initfile,pu k0))

############################################################################
#  p9_hcd_cache_dcc_skewdajust_setup
############################################################################
$(call BUILD_QUADCLK_INITFILE,p9.c_clk_dcadso_cntrl_leaf.scan.initfile,pu.eq k0:n0:s0:pu,clk,$(STEPS_dcadso_dd1),$(STEPS_dcadso_dd2))
$(call BUILD_QUADCLK_INITFILE,p9.c_clk_skew6w_cntrl_leaf.scan.initfile,pu.eq k0:n0:s0:pu,clk,$(STEPS_skew6w_dd1),$(STEPS_skew6w_dd2))

###############################################################################
# process filter pll initfiles (with buckets)
###############################################################################
$(eval $(call BUILD_FILTER_PLL_INITFILE,p9.filter.pll.override.scan.initfile,pu))

############################################################################
# Build overlay initfile - chip ec specific
#    This macro iterates over all chip and ec levels and will produce a
#    procedure which runs in the wrapper to generate an overlay ring.
#
# $1 == initfile name
# $2 == space separated list of targets
#
# NOTE: Overlays not supported for Nimbus DD1
############################################################################
define BUILD_OVERLAY_INITFILE
$(foreach chip_ec_pair, $(filter-out p9:p9n:10,$(filter p9%, $(CHIP_EC_PAIRS))),\
            $(eval $(call ADD_SCAN_INPUT_RULE,overlays,$(1),$(2), \
			$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair))))))
endef

###########################################################################
# Process all initfiles which generate overlay rings
###########################################################################
$(eval $(call BUILD_OVERLAY_INITFILE,p9.chip.gptr.overlays.scan.initfile,pu k0))
$(eval $(call BUILD_OVERLAY_INITFILE,p9.core.gptr.overlays.scan.initfile,pu.c k0 k0:n0:s0:pu))
$(eval $(call BUILD_OVERLAY_INITFILE,p9.eq.gptr.overlays.scan.initfile,pu.eq k0 k0:n0:s0:pu))
$(eval $(call BUILD_OVERLAY_INITFILE,p9.ex.gptr.overlays.scan.initfile,pu.ex k0 k0:n0:s0:pu))

###############################################################################
# BUILD_CENTAUR_INITFILE
#    This macro iterates over all build phases required for a given Centaur
#    initfile and adds an entry to CENTAUR_SCAN_INPUTS for each phase
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
###############################################################################
CENTAUR_INIT_PHASES = $(IPL_BASE) $(IPL_RISK)
define BUILD_CENTAUR_INITFILE
$(foreach chip_ec_pair,$(filter centaur%,$(CHIP_EC_PAIRS)),\
        $(foreach phase, $(CENTAUR_INIT_PHASES),\
                 $(eval $(call ADD_SCAN_INPUT_RULE,$(phase),$(1),$(2),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair)))))))

endef

###############################################################################
# BUILD_CENTAUR_PLL_INITFILE
#    This macro iterates over all operating frequency points for a given
#    initfile (operating point defined by bucket type/number) and adds an
#    entry to SCAN_INPUTS for each phase
# 
#    This macro is to be used on centaur.*.pll.scan.initfile
#
#    Input:
#      $1 == initfile name
#      $2 == space separated list of targets required to process initfile
#      $3 == bucket type (nest/mem/clk/l2)
###############################################################################
BUCKETS_CEN = 1 2 3 4 5 6 7 8

define BUILD_CENTAUR_PLL_INITFILE
$(foreach chip_ec_pair,$(filter centaur%,$(CHIP_EC_PAIRS)),\
        $(foreach bucket, $(BUCKETS_CEN),\
		$(eval $(call ADD_PLL_SCAN_INPUT_RULE,$(1),$(2),$(3),$(bucket),$(word 1,$(subst :, ,$(chip_ec_pair))),$(word 3,$(subst :, ,$(chip_ec_pair)))))))
endef


############################################################################
# Process Centaur scan initfiles
############################################################################
$(eval $(call BUILD_CENTAUR_PLL_INITFILE,centaur.cleanup.pll.scan.initfile,centaur k0,cen))
$(eval $(call BUILD_CENTAUR_PLL_INITFILE,centaur.mem.pll.scan.initfile,centaur k0,cen))
$(eval $(call BUILD_CENTAUR_PLL_INITFILE,centaur.nest.pll.scan.initfile,centaur k0,cen))

$(eval $(call BUILD_CENTAUR_INITFILE,centaur.mba.scan.initfile,centaur))
$(eval $(call BUILD_CENTAUR_INITFILE,centaur.mbs.scan.initfile,centaur k0))
$(eval $(call BUILD_CENTAUR_INITFILE,centaur.dmi.scan.initfile,centaur k0))
$(eval $(call BUILD_CENTAUR_INITFILE,centaur.thermal.scan.initfile,centaur k0))
