# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/ifCompiler/plat/fapi2CreateChipEcFeatureWrapper.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2016
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG
GENERATED=plat_create_chip_ec_feature_wrapper_ifCompiler
$(GENERATED)_COMMAND_PATH=$(ROOTPATH)/tools/ifCompiler/plat/
COMMAND=fapi2CreateChipEcFeatureWrapper.pl

SOURCES+=$(GENPATH)/fapi2_ifCompiler/attribute_ids.H
SOURCES+=$(GENPATH)/fapi2_ifCompiler/fapi2_chip_ec_feature.H
TARGETS+=plat_chip_ec_feature.H

$(GENERATED)_PATH=$(GENPATH)/fapi2_ifCompiler

define plat_create_chip_ec_feature_wrapper_ifCompiler_RUN
		$(C1) $$< -i $(GENPATH) -o $$($(GENERATED)_PATH)
endef
$(call BUILD_GENERATED)
