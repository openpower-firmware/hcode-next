# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/build/ekb.dir/chips.env.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2018
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

# Lists of chip subdirectories.
CHIPS += p9
CHIPS += centaur
CHIPS += ocmb
CHIPS += common
CHIPS += ocmb/explorer
CHIPS += p9a

p9_CHIPID += p9n
p9_CHIPID += p9c
p9_CHIPID += p9a
centaur_CHIPID += cen
ocmb_CHIPID += explorer

p9n_EC += 10 20 21 22 23
p9c_EC += 10 11 12 13
p9a_EC += 10
cen_EC += 20
explorer_EC += 10

HW_IMAGE_VARIATIONS = hw sim
