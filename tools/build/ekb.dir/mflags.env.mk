# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/build/ekb.dir/mflags.env.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2018
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

# Makefile to contain general default variables for the make environment.

# Default output paths.
#ROOTPATH=../..
ROOTPATH=../../import
OUTPUTPATH?=$(ROOTPATH)/output
LIBPATH?=$(OUTPUTPATH)/lib
EXEPATH?=$(OUTPUTPATH)/bin
OBJPATH?=$(OUTPUTPATH)/obj
GENPATH?=$(OUTPUTPATH)/gen
IMAGEPATH?=$(OUTPUTPATH)/images
XIPPATH?=$(ROOTPATH)/chips/p9/xip

# Location of the cross-compiler toolchain.
UNAME = $(shell uname)
__EKB_PREFIX?=/opt/rh/devtoolset-2/root/usr/bin/

ifeq ($(UNAME),AIX)
__EKB_PREFIX=/opt/xsite/contrib/bin/
endif

HOST_PREFIX?=$(__EKB_PREFIX)
TARGET_PREFIX?=$(__EKB_PREFIX)

# Location of PPE42 cross-compiler toolchain
PPE_TOOL_PATH ?= $(CTEPATH)/tools/ppetools/prod
PPE_PREFIX    ?= $(PPE_TOOL_PATH)/bin/powerpc-eabi-
PPE_BINUTILS_PREFIX ?= $(PPE_TOOL_PATH)/powerpc-eabi/bin/

# Default compiler tools to use.
CC?=gcc
CXX?=g++
AR?=ar
LD?=ld
