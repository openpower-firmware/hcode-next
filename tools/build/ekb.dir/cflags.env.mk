# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/build/ekb.dir/cflags.env.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2016
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG

# Makefile for additional compile flags.

# Force 64 bit compile and link.
ifeq ( $(UNAME), Linux )
LOCALCOMMONFLAGS += -m64
endif
CFLAGS += -O0
