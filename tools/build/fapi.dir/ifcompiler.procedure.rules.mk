# IBM_PROLOG_BEGIN_TAG
# This is an automatically generated prolog.
#
# $Source: tools/build/fapi.dir/ifcompiler.procedure.rules.mk $
#
# IBM CONFIDENTIAL
#
# EKB Project
#
# COPYRIGHT 2015,2016
# [+] International Business Machines Corp.
#
#
# The source code for this program is not published or otherwise
# divested of its trade secrets, irrespective of what has been
# deposited with the U.S. Copyright Office.
#
# IBM_PROLOG_END_TAG
# Settings needed for FAPI2_ifCompiler procedures
#    * Add $(GENPATH)/fapi2_ifCompiler to the source directory
#    * Add ifCompiler plat directory to the include directory
#    * Add engd_parser to the include directory
#    * Add ecmd files to the include directory
#    * Add common fapi2 files to the include directory
#    * Add dependency on ecmd libraries
#    * Add dependency on fapi2_ifCompiler module
#    * Add dependency on engd_parser module
#    * Add dependency on libdl.so
#    * Set FAPI_SUPPORT_SPY_AS_STRING = 1
define FAPI2_IFCOMPILER_PROCEDURE
$(call ADD_MODULE_SRCDIR,$(PROCEDURE),$(GENPATH)/fapi2_ifCompiler)
$(call __ADD_MODULE_INCDIR,$(PROCEDURE),$(ROOTPATH)/tools/ifCompiler/plat)
$(call __ADD_MODULE_INCDIR,$(PROCEDURE),$(ROOTPATH)/tools/ifCompiler/engd_parser/include)
$(call __ADD_MODULE_INCDIR,$(PROCEDURE),$(ECMD_PLAT_INCLUDE))
$(call __ADD_MODULE_INCDIR,$(PROCEDURE),$(FAPI2_PATH)/include)
$(call __ADD_MODULE_INCDIR,$(PROCEDURE),$(ROOTPATH)/chips/p9/procedures/hwp/ffdc/)
$(call __ADD_MODULE_INCDIR,$(PROCEDURE),$(ROOTPATH)/chips/p9/common/include/)
lib$(PROCEDURE)_EXTRALIBS += $(ECMD_REQUIRED_LIBS)
lib$(PROCEDURE)_DEPLIBS += fapi2_ifcompiler
lib$(PROCEDURE)_DEPLIBS += engd_parser
lib$(PROCEDURE)_LDFLAGS += -ldl
lib$(PROCEDURE)_COMMONFLAGS+=-DFAPI_SUPPORT_SPY_AS_STRING=1
lib$(PROCEDURE)_COMMONFLAGS+=-DIFCOMPILER_PLAT=1
lib$(PROCEDURE)_COMMONFLAGS+=-DPLAT_NO_THREAD_LOCAL_STORAGE=1
lib$(PROCEDURE)_LIBPATH=$(LIBPATH)/ifCompiler
endef
